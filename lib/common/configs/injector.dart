import 'package:connectivity/connectivity.dart';
import 'package:demo_app/common/configs/injector.iconfig.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart';

final getIt = GetIt.instance;

@injectableInit
Future<void> configure() async {
  $initGetIt(getIt);
  final SharedPreferences sharedPreferences =
      await SharedPreferences.getInstance();
  getIt.registerLazySingleton(() => Client());
  getIt.registerLazySingleton(() => Connectivity());
  getIt.registerLazySingleton(() => sharedPreferences);
}
