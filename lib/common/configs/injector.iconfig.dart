// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:demo_app/common/utils/search_cache.dart';
import 'package:demo_app/data/splash/datasources/splash_local_data_source_impl.dart';
import 'package:demo_app/data/splash/datasources/splash_local_data_source.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:demo_app/data/splash/repositories/splash_repository_impl.dart';
import 'package:demo_app/domain/splash/repositories/splash_repository.dart';
import 'package:demo_app/domain/splash/usecases/check_is_authenticated.dart';
import 'package:demo_app/data/login/datasources/login_local_data_source_impl.dart';
import 'package:demo_app/data/login/datasources/login_local_data_source.dart';
import 'package:demo_app/data/home/datasources/home_local_data_source_impl.dart';
import 'package:demo_app/data/home/datasources/home_local_data_source.dart';
import 'package:demo_app/data/home/repositories/home_repository_impl.dart';
import 'package:demo_app/domain/home/repositories/home_repository.dart';
import 'package:demo_app/domain/home/usecases/remove_current_token.dart';
import 'package:demo_app/presentation/home/blocs/home_bloc.dart';
import 'package:demo_app/common/networks/network_info.dart';
import 'package:connectivity/connectivity.dart';
import 'package:demo_app/data/dashboard/datasources/dashboard_remote_data_source_impl.dart';
import 'package:demo_app/data/dashboard/datasources/dashboard_remote_data_source.dart';
import 'package:http/http.dart';
import 'package:demo_app/data/dashboard/repositories/dashboard_repository_impl.dart';
import 'package:demo_app/domain/dashboard/repositories/dashboard_repository.dart';
import 'package:demo_app/domain/dashboard/usecases/get_recommended_movie.dart';
import 'package:demo_app/data/favorite_list/datasources/favorite_list_remote_data_source_impl.dart';
import 'package:demo_app/data/favorite_list/datasources/favorite_list_remote_data_source.dart';
import 'package:demo_app/data/favorite_list/repositories/favorite_list_repository_impl.dart';
import 'package:demo_app/domain/favorite_list/repositories/dashboard_repository.dart';
import 'package:demo_app/domain/favorite_list/usecases/get_favorite_movies.dart';
import 'package:demo_app/presentation/favorite_list/blocs/favorite_list_bloc.dart';
import 'package:demo_app/presentation/dashboard/blocs/dashboard_bloc.dart';
import 'package:demo_app/data/search/datasources/search_remote_data_source_impl.dart';
import 'package:demo_app/data/search/datasources/search_remote_data_source.dart';
import 'package:demo_app/data/search/repositories/search_repository_impl.dart';
import 'package:demo_app/domain/search/respositories/search_repository.dart';
import 'package:demo_app/domain/search/usecases/search_movies.dart';
import 'package:demo_app/presentation/search/blocs/search_bloc.dart';
import 'package:demo_app/data/favorite_detail/datasources/favorite_detail_remote_data_source_impl.dart';
import 'package:demo_app/data/favorite_detail/datasources/favorite_detail_remote_data_source.dart';
import 'package:demo_app/data/favorite_detail/repositories/favorite_detail_repository_impl.dart';
import 'package:demo_app/domain/favorite_detail/repositories/favorite_detail_repository.dart';
import 'package:demo_app/domain/favorite_detail/usecases/add_or_update_movie_detail.dart';
import 'package:demo_app/domain/favorite_detail/usecases/update_current_movie_detail.dart';
import 'package:demo_app/presentation/favorite_detail/blocs/favorite_detail_bloc.dart';
import 'package:demo_app/data/settings/datasources/settings_local_data_source_impl.dart';
import 'package:demo_app/data/settings/datasources/settings_local_data_source.dart';
import 'package:demo_app/data/settings/repositories/settings_repository_impl.dart';
import 'package:demo_app/domain/settings/repositories/settings_repository.dart';
import 'package:demo_app/domain/settings/usecases/get_show_recommended.dart';
import 'package:demo_app/domain/settings/usecases/set_show_recommended.dart';
import 'package:demo_app/presentation/settings/blocs/settings_bloc.dart';
import 'package:demo_app/data/login/repositories/login_repository_impl.dart';
import 'package:demo_app/domain/login/repositories/login_repository.dart';
import 'package:demo_app/presentation/splash/blocs/splash_bloc.dart';
import 'package:demo_app/domain/login/usecases/save_the_token.dart';
import 'package:demo_app/domain/login/usecases/try_to_login.dart';
import 'package:demo_app/presentation/login/blocs/login_bloc.dart';
import 'package:get_it/get_it.dart';

void $initGetIt(GetIt getIt, {String environment}) {
  getIt
    ..registerLazySingleton<SearchCache>(() => SearchCache())
    ..registerLazySingleton<SplashLocalDataSource>(() => SplashLocalDataSourceImpl(
        sharedPreferences: getIt<SharedPreferences>()))
    ..registerLazySingleton<SplashRepository>(() => SplashRepositoryImpl(
        splashLocalDataSource: getIt<SplashLocalDataSource>()))
    ..registerLazySingleton<CheckIsAuthenticated>(
        () => CheckIsAuthenticated(splashRepository: getIt<SplashRepository>()))
    ..registerLazySingleton<LoginLocalDataSource>(() =>
        LoginLocalDataSourceImpl(sharedPreferences: getIt<SharedPreferences>()))
    ..registerLazySingleton<HomeLocalDataSource>(() =>
        HomeLocalDataSourceImpl(sharedPreferences: getIt<SharedPreferences>()))
    ..registerLazySingleton<HomeRepository>(() =>
        HomeRepositoryImpl(homeLocalDataSource: getIt<HomeLocalDataSource>()))
    ..registerLazySingleton<RemoveCurrentToken>(
        () => RemoveCurrentToken(homeRepository: getIt<HomeRepository>()))
    ..registerLazySingleton<HomeBloc>(
        () => HomeBloc(removeCurrentToken: getIt<RemoveCurrentToken>()))
    ..registerLazySingleton<NetworkInfo>(
        () => NetworkInfoImpl(connectivity: getIt<Connectivity>()))
    ..registerLazySingleton<DashboardRemoteDataSource>(() =>
        DashboardRemoteDataSourceImpl(
            httpClient: getIt<Client>(),
            sharedPreferences: getIt<SharedPreferences>()))
    ..registerLazySingleton<DashboardRepository>(() => DashboardRepositoryImpl(dashboardRemoteDataSource: getIt<DashboardRemoteDataSource>(), networkInfo: getIt<NetworkInfo>()))
    ..registerLazySingleton<GetRecommendedMovie>(() => GetRecommendedMovie(dashboardRepository: getIt<DashboardRepository>()))
    ..registerLazySingleton<FavoriteListRemoteDataSource>(() => FavoriteListRemoteDataSourceImpl(httpClient: getIt<Client>(), sharedPreferences: getIt<SharedPreferences>()))
    ..registerLazySingleton<FavoriteListRepository>(() => FavoriteListRepositoryImpl(favoriteListRemoteDataSource: getIt<FavoriteListRemoteDataSource>(), networkInfo: getIt<NetworkInfo>()))
    ..registerLazySingleton<GetFavoriteMovies>(() => GetFavoriteMovies(favoriteListRepository: getIt<FavoriteListRepository>()))
    ..registerLazySingleton<FavoriteListBloc>(() => FavoriteListBloc(getFavoriteMovies: getIt<GetFavoriteMovies>()))
    ..registerLazySingleton<DashboardBloc>(() => DashboardBloc(getRecommendedMovie: getIt<GetRecommendedMovie>(), favoriteListBloc: getIt<FavoriteListBloc>()))
    ..registerLazySingleton<SearchRemoteDataSource>(() => SearchRemoteDataSourceImpl(httpClient: getIt<Client>()))
    ..registerLazySingleton<SearchRepository>(() => SearchRepositoryImpl(
          searchCache: getIt<SearchCache>(),
          searchRemoteDataSource: getIt<SearchRemoteDataSource>(),
          networkInfo: getIt<NetworkInfo>(),
        ))
    ..registerLazySingleton<SearchMovies>(() => SearchMovies(searchRepository: getIt<SearchRepository>()))
    ..registerLazySingleton<SearchBloc>(() => SearchBloc(searchMovies: getIt<SearchMovies>()))
    ..registerLazySingleton<FavoriteDetailRemoteDataSource>(() => FavoriteDetailRemoteDataSourceImpl(httpClient: getIt<Client>(), sharedPreferences: getIt<SharedPreferences>()))
    ..registerLazySingleton<FavoriteDetailRepository>(() => FavoriteDetailRepositoryImpl(favoriteDetailDataSource: getIt<FavoriteDetailRemoteDataSource>(), networkInfo: getIt<NetworkInfo>()))
    ..registerLazySingleton<AddOrUpdateMovieDetail>(() => AddOrUpdateMovieDetail(favoriteDetailRepository: getIt<FavoriteDetailRepository>()))
    ..registerLazySingleton<UpdateCurrentMovieDetail>(() => UpdateCurrentMovieDetail(favoriteDetailRepository: getIt<FavoriteDetailRepository>()))
    ..registerLazySingleton<FavoriteDetailBloc>(() => FavoriteDetailBloc(
          addOrUpdateMovieDetail: getIt<AddOrUpdateMovieDetail>(),
          updateCurrentMovieDetail: getIt<UpdateCurrentMovieDetail>(),
          favoriteListBloc: getIt<FavoriteListBloc>(),
        ))
    ..registerLazySingleton<SettingsLocalDataSource>(() => SettingsLocalDataSourceImpl(sharedPreferences: getIt<SharedPreferences>()))
    ..registerLazySingleton<SettingsRepository>(() => SettingsRepositoryImpl(settingsLocalDataSource: getIt<SettingsLocalDataSource>()))
    ..registerLazySingleton<GetShowRecommended>(() => GetShowRecommended(settingsRepository: getIt<SettingsRepository>()))
    ..registerLazySingleton<SetShowRecommended>(() => SetShowRecommended(settingsRepository: getIt<SettingsRepository>()))
    ..registerLazySingleton<SettingsBloc>(() => SettingsBloc(getShowRecommended: getIt<GetShowRecommended>(), setShowRecommended: getIt<SetShowRecommended>()))
    ..registerLazySingleton<LoginRepository>(() => LoginRepositoryImpl(loginLocalDataSource: getIt<LoginLocalDataSource>()))
    ..registerLazySingleton<SplashBloc>(() => SplashBloc(checkIsAuthenticated: getIt<CheckIsAuthenticated>(), dashboardBloc: getIt<DashboardBloc>()))
    ..registerLazySingleton<SaveTheToken>(() => SaveTheToken(loginRepository: getIt<LoginRepository>()))
    ..registerLazySingleton<TryToLogin>(() => TryToLogin(loginRepository: getIt<LoginRepository>()))
    ..registerLazySingleton<LoginBloc>(() => LoginBloc(
          tryToLogin: getIt<TryToLogin>(),
          saveTheToken: getIt<SaveTheToken>(),
          dashboardBloc: getIt<DashboardBloc>(),
          settingsBloc: getIt<SettingsBloc>(),
        ));
}
