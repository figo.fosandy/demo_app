import 'package:connectivity/connectivity.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

abstract class NetworkInfo {
  Future<bool> isConnected();
}

@RegisterAs(NetworkInfo)
@lazySingleton
@injectable
class NetworkInfoImpl implements NetworkInfo {
  final Connectivity connectivity;

  const NetworkInfoImpl({@required this.connectivity});

  @override
  Future<bool> isConnected() async {
    ConnectivityResult connectivityResult =
        await connectivity.checkConnectivity();
    if (connectivityResult == ConnectivityResult.none) {
      return false;
    }
    return true;
  }
}
