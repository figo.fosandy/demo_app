abstract class Routes {
  const Routes();
  static const String splash = '/splash';
  static const String login = '/login';
  static const String _homePath = 'home';
  static const String home = '/$_homePath';
  static const String _dashboardPath = 'dashboard';
  static const String dashboard = '$home/$_dashboardPath';
  static const String _searchPath = 'search';
  static const String search = '$home/$_searchPath';
  static const String _favoriteListPath = 'favoriteList';
  static const String favoriteList = '$home/$_favoriteListPath';
  static const String _settingsPath = 'settings';
  static const String settings = '$home/$_settingsPath';
  static const String _favoriteDetailsPath = 'favoriteDetails';
  static const String favoriteDetails = '/$_favoriteDetailsPath';
  static const String _loadingPath = 'loading';
  static const String loading = '/$_loadingPath';
}
