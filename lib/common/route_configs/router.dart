import 'package:demo_app/common/route_configs/routes.dart';
import 'package:demo_app/presentation/favorite_detail/pages/favorite_detail_page.dart';
import 'package:demo_app/presentation/home/pages/home_page.dart';
import 'package:demo_app/presentation/login/pages/login_page.dart';
import 'package:demo_app/presentation/splash/pages/splash_page.dart';
import 'package:flutter/material.dart';

abstract class Router {
  const Router();
  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;

    switch (settings.name) {
      case Routes.splash:
        return MaterialPageRoute(builder: (_) => SplashPage());
        break;
      case Routes.home:
        return MaterialPageRoute(builder: (_) => HomePage());
        break;
      case Routes.dashboard:
        return PageRouteBuilder(
            pageBuilder: (context, animation1, animation2) =>
                HomePage(currentPageIndex: 0));
        break;
      case Routes.search:
        return PageRouteBuilder(
            pageBuilder: (context, animation1, animation2) =>
                HomePage(currentPageIndex: 1));
        break;
      case Routes.favoriteList:
        return PageRouteBuilder(
            pageBuilder: (context, animation1, animation2) =>
                HomePage(currentPageIndex: 2));
        break;
      case Routes.settings:
        return PageRouteBuilder(
            pageBuilder: (context, animation1, animation2) =>
                HomePage(currentPageIndex: 3));
        break;
      case Routes.login:
        return MaterialPageRoute(builder: (_) => LoginPage());
        break;
      case Routes.favoriteDetails:
        return MaterialPageRoute(
            builder: (_) => FavoriteDetailPage(movie: Map.from(args)['movie']));
      default:
        return null;
    }
  }
}
