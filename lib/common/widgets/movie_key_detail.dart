import 'package:flutter/material.dart';
import 'package:marquee_widget/marquee_widget.dart';

extension on Widget {
  Widget addMarquee() => Marquee(child: this);

  Widget addPadding({@required EdgeInsets padding}) => Padding(
        child: this,
        padding: padding,
      );
}

extension on bool {
  bool isNull() => this == null;
}

extension on int {
  Widget getStarIcon({@required int num}) => Icon(
        this - num > 1
            ? Icons.star
            : this - num == 1 ? Icons.star_half : Icons.star_border,
        color: Colors.cyan,
      );

  Widget getRating() => Row(
        children: <Widget>[
          this.getStarIcon(num: 0),
          this.getStarIcon(num: 2),
          this.getStarIcon(num: 4),
          this.getStarIcon(num: 6),
          this.getStarIcon(num: 8),
        ],
      ).addMarquee();
}

class MovieKeyDetail extends StatelessWidget {
  final String pairKey;
  final String pairValue;
  static const double basePaddingSize = 3;
  final bool isRating;

  const MovieKeyDetail({
    Key key,
    @required this.pairKey,
    @required this.pairValue,
    this.isRating,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _pairKeyWidget(),
        _pairValueWidget().addPadding(
            padding:
                const EdgeInsets.symmetric(horizontal: basePaddingSize * 4))
      ],
    ).addPadding(padding: const EdgeInsets.only(top: basePaddingSize * 2));
  }

  Widget _pairKeyWidget() {
    return Text(
      pairKey,
      style: TextStyle(
        color: Colors.white,
        fontWeight: FontWeight.bold,
        fontSize: 13,
      ),
    ).addPadding(padding: EdgeInsets.only(bottom: basePaddingSize));
  }

  Widget _pairValueWidget() {
    if (isRating.isNull() || !isRating) {
      return Text(
        pairValue,
        style: TextStyle(
          color: Colors.cyan,
          fontSize: 15,
        ),
      ).addMarquee();
    }
    return int.parse(pairValue).getRating();
  }
}
