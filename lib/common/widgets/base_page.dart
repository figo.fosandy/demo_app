import 'package:flutter/material.dart';

extension on String {
  bool isNotNull() => this != null;
}

class BasePage extends StatelessWidget {
  const BasePage();

  String pageName() => null;
  Widget body(BuildContext context) => Container();
  Key pageKey() => UniqueKey();
  Widget drawer(BuildContext context) => null;
  Widget bottomNavigationBar(BuildContext context) => null;
  Widget title() => Text(pageName());
  bool useDynamicAppBar() => false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: pageKey(),
        appBar: pageName().isNotNull() || useDynamicAppBar()
            ? AppBar(
                title: title(),
              )
            : null,
        body: body(context),
        drawer: drawer(context),
        bottomNavigationBar: bottomNavigationBar(context),
      ),
    );
  }
}
