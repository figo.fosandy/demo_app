import 'package:flutter/material.dart';

extension on Widget {
  Widget addBackgroundColor({@required bool isGreen}) => Container(
        child: this,
        padding: EdgeInsets.all(5),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: isGreen ? Colors.green : Colors.orange,
        ),
      );
}

class MessageBar extends StatelessWidget {
  final bool viewed;

  const MessageBar({
    Key key,
    @required this.viewed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Icon(Icons.visibility),
        Text(' ${viewed ?? viewed ?? false ? 'V' : 'Unv'}iewed'),
      ],
    ).addBackgroundColor(isGreen: viewed ?? viewed ?? false);
  }
}
