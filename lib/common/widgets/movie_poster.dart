import 'package:cached_network_image/cached_network_image.dart';
import 'package:demo_app/common/widgets/loading_indicator.dart';
import 'package:flutter/material.dart';

extension on Widget {
  Widget addClipRRect() => ClipRRect(
        borderRadius: BorderRadius.only(
          topLeft: const Radius.circular(5),
          bottomLeft: const Radius.circular(5),
        ),
        child: this,
      );

  Widget addCenter() => Container(
        child: this,
        alignment: Alignment.center,
        width: 175,
        color: Colors.red,
      );
}

class MoviePoster extends StatelessWidget {
  final String posterUrl;
  final double posterWidth;

  const MoviePoster({
    Key key,
    @required this.posterUrl,
    this.posterWidth,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: posterUrl,
      placeholder: (context, url) => Container(
        child: LoadingIndicatorWithoutScafold(),
        width: posterWidth ?? 175,
      ),
      width: posterWidth ?? 175,
      errorWidget: (context, url, error) => Icon(Icons.error).addCenter(),
      alignment: Alignment.centerLeft,
      height: 250,
      fit: BoxFit.fitHeight,
    ).addClipRRect();
  }
}
