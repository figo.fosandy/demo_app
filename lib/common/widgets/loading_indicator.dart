import 'package:demo_app/common/widgets/base_page.dart';
import 'package:flutter/material.dart';
import 'package:loading/indicator/ball_spin_fade_loader_indicator.dart';
import 'package:loading/loading.dart';
import 'package:marquee_widget/marquee_widget.dart';

extension on Widget {
  Widget addAlign({AlignmentGeometry alignment}) {
    return Align(
      alignment: alignment,
      child: this,
    );
  }

  Widget addMarquee() {
    return Marquee(
      directionMarguee: DirectionMarguee.oneDirection,
      child: this,
    );
  }

  Widget addMargin({EdgeInsets margin}) {
    return Container(
      margin: margin,
      child: this,
    );
  }

  Widget addBackgroundColor({Color color}) {
    return Container(
      color: color,
      child: this,
    );
  }
}

class LoadingIndicator extends BasePage {
  final String message;
  final Color backgroundColor;
  final Color baseColor;

  LoadingIndicator({
    this.message,
    this.backgroundColor,
    this.baseColor,
  });

  @override
  Key pageKey() => Key('loadingPage');

  @override
  Widget body(BuildContext context) {
    return LoadingIndicatorWithoutScafold(
      backgroundColor: backgroundColor,
      baseColor: baseColor,
      message: message,
    );
  }
}

class LoadingIndicatorWithoutScafold extends StatelessWidget {
  final String message;
  final Color backgroundColor;
  final Color baseColor;

  const LoadingIndicatorWithoutScafold({
    this.message,
    this.backgroundColor,
    this.baseColor,
  });

  @override
  Widget build(BuildContext context) {
    return _indicatorWithMessage().addBackgroundColor(color: backgroundColor);
  }

  Widget _indicatorWithMessage() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        _indicator()
            .addAlign(alignment: Alignment.center)
            .addMargin(margin: EdgeInsets.only(bottom: 25)),
        _textMessage()
            .addAlign(alignment: Alignment.center)
            .addMarquee()
            .addMargin(margin: EdgeInsets.symmetric(horizontal: 25)),
      ],
    );
  }

  Widget _indicator() {
    return Loading(
      indicator: BallSpinFadeLoaderIndicator(),
      color: baseColor ?? Colors.purpleAccent,
      size: 100,
    );
  }

  Widget _textMessage() {
    return Text(
      '${message ?? 'Loading'}, please wait...',
      style: TextStyle(color: baseColor ?? Colors.purpleAccent),
    );
  }
}
