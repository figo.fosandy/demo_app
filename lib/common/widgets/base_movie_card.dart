import 'package:demo_app/common/configs/injector.dart';
import 'package:demo_app/common/models/movie_model.dart';
import 'package:demo_app/common/route_configs/routes.dart';
import 'package:demo_app/common/widgets/message_bar.dart';
import 'package:demo_app/common/widgets/movie_key_detail.dart';
import 'package:demo_app/common/widgets/movie_poster.dart';
import 'package:demo_app/presentation/favorite_detail/blocs/blocs.dart';
import 'package:flutter/material.dart';

extension on Widget {
  Widget set250Height() => Container(
        height: 250,
        alignment: Alignment.centerLeft,
        child: this,
      );

  Widget addInkWel({@required void Function() onTap}) => InkWell(
        child: this,
        onTap: onTap,
      );

  Widget addFlexible() => Flexible(child: this);

  Widget addPadding({@required EdgeInsets padding}) => Padding(
        child: this,
        padding: padding,
      );

  Widget addVisibility({@required bool visible}) => Visibility(
        child: this,
        visible: visible,
      );

  Widget addStack({@required Widget messageBar}) => Stack(
        children: <Widget>[
          this,
          Positioned(
            child: messageBar,
            top: 4,
            left: 4,
          ),
        ],
      );
}

const List<String> priorityList = ['High', 'Medium', 'Low'];

class BaseMovieCard extends StatelessWidget {
  final MovieModel movieModel;
  final bool hideId;
  final bool hideLabel;
  final bool hidePriority;
  final bool hideRating;
  final bool showViewed;
  final bool onDetailPage;

  const BaseMovieCard({
    Key key,
    @required this.movieModel,
    bool hideId,
    bool hideLabel,
    bool hidePriority,
    bool hideRating,
    bool showViewed,
    bool onDetailPage,
  })  : this.hideId = hideId ?? false,
        this.hideLabel = hideLabel ?? false,
        this.hidePriority = hidePriority ?? false,
        this.hideRating = hideRating ?? false,
        this.showViewed = showViewed ?? false,
        this.onDetailPage = onDetailPage ?? false,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Row(
        children: <Widget>[
          MoviePoster(posterUrl: movieModel.poster)
              .addPadding(padding: const EdgeInsets.only(right: 10)),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 10),
              MovieKeyDetail(
                pairKey: 'Title',
                pairValue: movieModel.title ?? '',
              ),
              MovieKeyDetail(
                pairKey: 'Movie Id',
                pairValue: movieModel.id ?? '',
              ).addVisibility(visible: !hideId),
              MovieKeyDetail(
                pairKey: 'Year',
                pairValue: movieModel.year ?? '',
              ),
              MovieKeyDetail(
                pairKey: 'Label',
                pairValue: movieModel.label ?? '',
              ).addVisibility(visible: !hideLabel),
              MovieKeyDetail(
                pairKey: 'Priority',
                pairValue: movieModel.priority == null
                    ? ''
                    : priorityList[
                        movieModel.priority > 2 ? 2 : movieModel.priority],
              ).addVisibility(visible: !hidePriority),
              MovieKeyDetail(
                pairKey: 'Rating',
                pairValue: movieModel.rating?.toString() ?? '0',
                isRating: true,
              ).addVisibility(visible: !hideRating ?? true),
            ],
          ).addFlexible(),
        ],
      ).set250Height(),
    )
        .addStack(
            messageBar: MessageBar(
          viewed: movieModel.viewed,
        ).addVisibility(visible: showViewed))
        .addInkWel(
            onTap: onDetailPage
                ? null
                : () {
                    getIt<FavoriteDetailBloc>()
                        .add(FavoriteDetailSetCurrentMovie(movie: movieModel));
                    Navigator.of(context).pushNamed(Routes.favoriteDetails,
                        arguments: {'movie': movieModel});
                  });
  }
}
