import 'package:flutter/material.dart';

extension on Widget {
  Widget addConstraint250height() => ConstrainedBox(
        constraints: BoxConstraints(minHeight: 250),
        child: this,
      );

  Widget addPadding({@required EdgeInsets padding}) => Padding(
        padding: padding,
        child: this,
      );

  Widget addAlignment({@required AlignmentGeometry align}) => Align(
        alignment: align,
        child: this,
      );
}

class MessagePage extends StatelessWidget {
  const MessagePage({Key key}) : super(key: key);

  Color baseColor() => Colors.cyan;

  String message() => 'Hi there,\nthis is me';

  IconData iconData() => Icons.pan_tool;

  Widget action(BuildContext context) => Container();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        _icon(),
        _text(),
        action(context).addAlignment(align: Alignment.center),
      ],
    ).addConstraint250height();
  }

  Widget _icon() {
    return Icon(
      iconData(),
      color: baseColor(),
      size: 100,
    );
  }

  Widget _text() {
    return Text(
      message(),
      softWrap: true,
      textAlign: TextAlign.center,
      style: TextStyle(color: baseColor()),
    ).addPadding(padding: const EdgeInsets.symmetric(horizontal: 10));
  }
}
