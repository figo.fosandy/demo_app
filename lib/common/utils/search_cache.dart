import 'package:demo_app/common/models/movie_model.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
@injectable
class SearchCache {
  final _cache = <String, List<MovieModel>>{};

  List<MovieModel> get(String keyword, String year) =>
      _cache['s=$keyword&y=$year'];

  void set(String keyword, String year, List<MovieModel> result) =>
      _cache['s=$keyword&y=$year'] = result;

  bool contains(String keyword, String year) =>
      _cache.containsKey('s=$keyword&y=$year');

  void remove(String keyword, String year) =>
      _cache.remove('s=$keyword&y=$year');
}
