import 'package:equatable/equatable.dart';

abstract class MovieModel extends Equatable {
  final String id;
  final String title;
  final String year;
  final String poster;
  final String label;
  final int priority;
  final bool viewed;
  final int rating;
  final int timestamp;

  const MovieModel({
    this.id,
    this.title,
    this.year,
    this.poster,
    this.label,
    this.priority,
    this.viewed,
    this.rating,
    this.timestamp,
  });

  @override
  List<Object> get props => [
        id,
        title,
        year,
        poster,
        label,
        priority,
        viewed,
        timestamp,
      ];
}
