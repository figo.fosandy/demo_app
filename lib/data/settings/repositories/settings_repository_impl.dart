import 'package:demo_app/data/settings/datasources/settings_local_data_source.dart';
import 'package:demo_app/domain/settings/repositories/settings_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@RegisterAs(SettingsRepository)
@injectable
@lazySingleton
class SettingsRepositoryImpl implements SettingsRepository {
  final SettingsLocalDataSource settingsLocalDataSource;

  const SettingsRepositoryImpl({@required this.settingsLocalDataSource});

  @override
  Future<void> setShowRecommended(bool showRecommended) async {
    return await settingsLocalDataSource.saveShowRecommended(showRecommended);
  }

  @override
  Future<bool> getShowRecommended() async {
    return await settingsLocalDataSource.getShowRecommended() ?? true;
  }
}
