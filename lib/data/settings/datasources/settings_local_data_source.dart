abstract class SettingsLocalDataSource {
  Future<void> saveShowRecommended(bool showRecommended);
  Future<bool> getShowRecommended();
}
