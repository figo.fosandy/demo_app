import 'package:demo_app/data/settings/datasources/settings_local_data_source.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

const SHOW_RECOMMENDED = 'SHOW_RECOMMENDED';
const TOKEN = 'TOKEN';

@RegisterAs(SettingsLocalDataSource)
@lazySingleton
@injectable
class SettingsLocalDataSourceImpl implements SettingsLocalDataSource {
  final SharedPreferences sharedPreferences;

  const SettingsLocalDataSourceImpl({@required this.sharedPreferences});

  @override
  Future<void> saveShowRecommended(bool showRecommended) async {
    final currentToken = sharedPreferences.getString(TOKEN);
    return await sharedPreferences.setBool(
        SHOW_RECOMMENDED + currentToken, showRecommended);
  }

  @override
  Future<bool> getShowRecommended() async {
    final currentToken = sharedPreferences.getString(TOKEN);
    return sharedPreferences.getBool(SHOW_RECOMMENDED + currentToken);
  }
}
