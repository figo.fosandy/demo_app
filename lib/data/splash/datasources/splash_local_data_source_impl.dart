import 'package:demo_app/data/splash/datasources/splash_local_data_source.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

const TOKEN = 'TOKEN';

@RegisterAs(SplashLocalDataSource)
@lazySingleton
@injectable
class SplashLocalDataSourceImpl implements SplashLocalDataSource {
  final SharedPreferences sharedPreferences;

  const SplashLocalDataSourceImpl({@required this.sharedPreferences});

  @override
  Future<String> getLastToken() async {
    return sharedPreferences.getString(TOKEN);
  }
}
