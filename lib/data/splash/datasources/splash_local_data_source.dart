abstract class SplashLocalDataSource {
  Future<String> getLastToken();
}