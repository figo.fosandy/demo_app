import 'package:demo_app/data/splash/datasources/splash_local_data_source.dart';
import 'package:demo_app/domain/splash/repositories/splash_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

extension on String {
  bool isValidToken() => this != null && this.isNotEmpty;
}

@RegisterAs(SplashRepository)
@lazySingleton
@injectable
class SplashRepositoryImpl implements SplashRepository {
  final SplashLocalDataSource splashLocalDataSource;

  const SplashRepositoryImpl({@required this.splashLocalDataSource});
  @override
  Future<bool> checkIsAuthenticated() async {
    final String lastToken = await splashLocalDataSource.getLastToken();
    return lastToken.isValidToken();
  }
}
