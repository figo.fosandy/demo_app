import 'package:demo_app/common/networks/network_info.dart';
import 'package:demo_app/data/dashboard/datasources/dashboard_remote_data_source.dart';
import 'package:demo_app/domain/dashboard/entities/dashboard_movie_entity.dart';
import 'package:demo_app/domain/dashboard/repositories/dashboard_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@RegisterAs(DashboardRepository)
@lazySingleton
@injectable
class DashboardRepositoryImpl implements DashboardRepository {
  final DashboardRemoteDataSource dashboardRemoteDataSource;
  final NetworkInfo networkInfo;

  const DashboardRepositoryImpl({
    @required this.dashboardRemoteDataSource,
    @required this.networkInfo,
  });

  @override
  Future<DashboardMovieEntity> getRecommendedMovie() async {
    if (await networkInfo.isConnected()) {
      return await dashboardRemoteDataSource.getRecommendedMovie();
    }
    throw ('Not connected to internet');
  }
}
