import 'package:demo_app/domain/dashboard/entities/dashboard_movie_entity.dart';

class DashboardMovieModel extends DashboardMovieEntity {
  const DashboardMovieModel({
    String id,
    String title,
    String year,
    String poster,
    String label,
    int priority,
    bool viewed,
    int rating,
    int timestamp,
  }) : super(
          id: id,
          title: title,
          year: year,
          poster: poster,
          label: label,
          priority: priority,
          viewed: viewed,
          rating: rating,
          timestamp: timestamp,
        );

  factory DashboardMovieModel.fromJson(Map<String, dynamic> json) =>
      DashboardMovieModel(
        id: json['id'],
        title: json['title'],
        year: json['year'],
        poster: json['poster'],
        label: json['label'],
        priority: json['priority'] as int,
        viewed: json['viewed'] as bool,
        rating: json['rating'] as int,
        timestamp: json['timestamp'] as int,
      );

  Map toJson() => {
        'id': id,
        'title': title,
        'year': year,
        'poster': poster,
        'label': label,
        'priority': priority,
        'viewed': viewed,
        'rating': rating,
        'timestamp': timestamp,
      };

  @override
  String toString() {
    return toJson().toString();
  }
}
