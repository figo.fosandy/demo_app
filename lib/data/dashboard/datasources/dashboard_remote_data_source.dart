import 'package:demo_app/data/dashboard/models/dashboard_movie_model.dart';

abstract class DashboardRemoteDataSource {
  Future<DashboardMovieModel> getRecommendedMovie();
}