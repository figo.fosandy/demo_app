import 'dart:convert';

import 'package:demo_app/data/dashboard/datasources/dashboard_remote_data_source.dart';
import 'package:demo_app/data/dashboard/models/dashboard_movie_model.dart';
import 'package:http/http.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

const TOKEN = 'TOKEN';

@RegisterAs(DashboardRemoteDataSource)
@lazySingleton
@injectable
class DashboardRemoteDataSourceImpl implements DashboardRemoteDataSource {
  final Client httpClient;
  final SharedPreferences sharedPreferences;
  final String _baseUrl = 'https://demo-video-ws-chfmsoli4q-ew.a.run.app';

  const DashboardRemoteDataSourceImpl({
    @required this.httpClient,
    @required this.sharedPreferences,
  });

  @override
  Future<DashboardMovieModel> getRecommendedMovie() async {
    final String token = sharedPreferences.getString(TOKEN);
    final Response response = await httpClient
        .get('$_baseUrl/video-ws/recommended', headers: {'token': token});
    final responseJson = json.decode(response.body);
    if (response.statusCode == 200) {
      return DashboardMovieModel.fromJson(responseJson);
    }
    throw (responseJson['error']);
  }
}
