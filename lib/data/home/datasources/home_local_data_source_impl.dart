import 'package:demo_app/data/home/datasources/home_local_data_source.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

const TOKEN = 'TOKEN';

@RegisterAs(HomeLocalDataSource)
@lazySingleton
@injectable
class HomeLocalDataSourceImpl implements HomeLocalDataSource {
  final SharedPreferences sharedPreferences;

  const HomeLocalDataSourceImpl({@required this.sharedPreferences});

  @override
  Future<void> removeCurrentToken() {
    return sharedPreferences.setString(TOKEN, '');
  }
}
