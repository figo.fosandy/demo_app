abstract class HomeLocalDataSource {
  Future<void> removeCurrentToken();
}
