import 'package:demo_app/data/home/datasources/home_local_data_source.dart';
import 'package:demo_app/domain/home/repositories/home_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@RegisterAs(HomeRepository)
@lazySingleton
@injectable
class HomeRepositoryImpl implements HomeRepository {
  final HomeLocalDataSource homeLocalDataSource;

  const HomeRepositoryImpl({@required this.homeLocalDataSource});

  @override
  Future<void> removeCurrentToken() {
    return homeLocalDataSource.removeCurrentToken();
  }
}
