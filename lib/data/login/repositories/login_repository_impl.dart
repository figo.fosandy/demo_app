import 'package:demo_app/data/login/datasources/login_local_data_source.dart';
import 'package:demo_app/domain/login/repositories/login_repository.dart';
import 'package:encrypt/encrypt.dart' as Encrypt;
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@RegisterAs(LoginRepository)
@injectable
@lazySingleton
class LoginRepositoryImpl implements LoginRepository {
  final LoginLocalDataSource loginLocalDataSource;

  const LoginRepositoryImpl({@required this.loginLocalDataSource});

  @override
  Future<String> tryToLogin(String token) async {
    if (token.isEmpty) {
      throw ('Token must be not empty');
    }
    final key = Encrypt.Key.fromLength(32);
    final iv = Encrypt.IV.fromLength(16);
    final encrypter = Encrypt.Encrypter(Encrypt.AES(key));
    return encrypter.encrypt(token, iv: iv).base64;
  }

  @override
  Future<void> saveTheToken(String token) async {
    return await loginLocalDataSource.saveTheToken(token);
  }
}
