abstract class LoginLocalDataSource {
  Future<void> saveTheToken(String token);
}
