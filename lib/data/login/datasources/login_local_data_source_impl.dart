import 'package:demo_app/data/login/datasources/login_local_data_source.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

const TOKEN = 'TOKEN';

@RegisterAs(LoginLocalDataSource)
@lazySingleton
@injectable
class LoginLocalDataSourceImpl implements LoginLocalDataSource {
  final SharedPreferences sharedPreferences;

  const LoginLocalDataSourceImpl({@required this.sharedPreferences});

  @override
  Future<void> saveTheToken(String token) {
    return sharedPreferences.setString(TOKEN, token);
  }
}
