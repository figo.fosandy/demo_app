import 'package:demo_app/common/models/movie_model.dart';
import 'package:demo_app/domain/favorite_detail/entities/favorite_detail_entity.dart';
import 'package:meta/meta.dart';

class FavoriteDetailModel extends FavoriteDetailEntity {
  const FavoriteDetailModel({
    @required String id,
    @required String title,
    @required String year,
    @required String poster,
    @required String label,
    @required int priority,
    @required bool viewed,
    @required int rating,
    @required int timestamp,
  }) : super(
          id: id,
          title: title,
          year: year,
          poster: poster,
          label: label,
          priority: priority,
          viewed: viewed,
          rating: rating,
          timestamp: timestamp,
        );

  factory FavoriteDetailModel.fromJson(Map<String, dynamic> json) =>
      FavoriteDetailModel(
        id: json['id'],
        title: json['title'],
        year: json['year'],
        poster: json['poster'],
        label: json['label'],
        priority: json['priority'] as int,
        viewed: json['viewed'] as bool,
        rating: json['rating'] as int,
        timestamp: json['timestamp'] as int,
      );

  factory FavoriteDetailModel.fromEntity(MovieModel entity) =>
      FavoriteDetailModel(
        id: entity.id,
        title: entity.title,
        year: entity.year,
        poster: entity.poster,
        label: entity.label,
        priority: entity.priority,
        viewed: entity.viewed,
        rating: entity.rating,
        timestamp: entity.timestamp,
      );

  Map toJson() => {
        'id': id,
        'title': title,
        'year': year,
        'poster': poster,
        'label': label,
        'priority': priority,
        'viewed': viewed,
        'rating': rating,
        'timestamp': timestamp,
      };

  FavoriteDetailModel copyWith({
    String label,
    int priority,
    bool viewed,
    int rating,
    int timestamp,
  }) =>
      FavoriteDetailModel(
        id: this.id,
        title: this.title,
        year: this.year,
        poster: this.poster,
        label: label ?? this.label,
        priority: priority ?? this.priority,
        viewed: viewed ?? this.viewed,
        rating: rating ?? this.rating,
        timestamp: timestamp ?? this.timestamp,
      );

  @override
  String toString() {
    return toJson().toString();
  }
}
