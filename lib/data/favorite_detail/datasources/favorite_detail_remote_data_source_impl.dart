import 'dart:convert';
import 'dart:io';

import 'package:demo_app/data/favorite_detail/datasources/favorite_detail_remote_data_source.dart';
import 'package:demo_app/data/favorite_detail/models/favorite_detail_model.dart';
import 'package:http/http.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

const TOKEN = 'TOKEN';

@RegisterAs(FavoriteDetailRemoteDataSource)
@lazySingleton
@injectable
class FavoriteDetailRemoteDataSourceImpl
    implements FavoriteDetailRemoteDataSource {
  final Client httpClient;
  final SharedPreferences sharedPreferences;
  final String _baseUrl = 'https://demo-video-ws-chfmsoli4q-ew.a.run.app';

  const FavoriteDetailRemoteDataSourceImpl({
    @required this.httpClient,
    @required this.sharedPreferences,
  });

  @override
  Future<void> updateMovieDetail(FavoriteDetailModel movie) async {
    final String token = sharedPreferences.getString(TOKEN);
    return httpClient.put(
      '$_baseUrl/video-ws/videos/${movie.id}',
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        HttpHeaders.acceptHeader: '*/*',
        'token': token,
      },
      body: json.encode(movie),
    );
  }
}
