import 'package:demo_app/data/favorite_detail/models/favorite_detail_model.dart';

abstract class FavoriteDetailRemoteDataSource {
  Future<void> updateMovieDetail(FavoriteDetailModel movie);
}
