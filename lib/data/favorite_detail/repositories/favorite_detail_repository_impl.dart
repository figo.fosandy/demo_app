import 'package:demo_app/common/models/movie_model.dart';
import 'package:demo_app/common/networks/network_info.dart';
import 'package:demo_app/data/favorite_detail/datasources/favorite_detail_remote_data_source.dart';
import 'package:demo_app/data/favorite_detail/models/favorite_detail_model.dart';
import 'package:demo_app/domain/favorite_detail/entities/favorite_detail_entity.dart';
import 'package:demo_app/domain/favorite_detail/repositories/favorite_detail_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@RegisterAs(FavoriteDetailRepository)
@lazySingleton
@injectable
class FavoriteDetailRepositoryImpl implements FavoriteDetailRepository {
  final FavoriteDetailRemoteDataSource favoriteDetailDataSource;
  final NetworkInfo networkInfo;

  const FavoriteDetailRepositoryImpl({
    @required this.favoriteDetailDataSource,
    @required this.networkInfo,
  });

  @override
  Future<void> addOrUpdateMovieDetail({MovieModel movieDetail}) async {
    if (await networkInfo.isConnected()) {
      return favoriteDetailDataSource
          .updateMovieDetail(FavoriteDetailModel.fromEntity(movieDetail));
    }
    throw ('Not conneceted to the internet');
  }

  @override
  FavoriteDetailEntity updateCurrentMovieDetail({
    MovieModel currentMovie,
    String label,
    int priority,
    int rating,
    bool viewed,
  }) {
    return FavoriteDetailModel.fromEntity(currentMovie).copyWith(
      label: label,
      priority: priority,
      rating: rating,
      viewed: viewed,
      timestamp: DateTime.now().millisecondsSinceEpoch,
    );
  }
}
