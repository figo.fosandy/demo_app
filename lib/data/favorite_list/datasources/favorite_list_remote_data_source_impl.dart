import 'dart:convert';

import 'package:demo_app/data/favorite_list/datasources/favorite_list_remote_data_source.dart';
import 'package:demo_app/data/favorite_list/models/favorite_list_movie_model.dart';
import 'package:http/http.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

const TOKEN = 'TOKEN';

@RegisterAs(FavoriteListRemoteDataSource)
@lazySingleton
@injectable
class FavoriteListRemoteDataSourceImpl implements FavoriteListRemoteDataSource {
  final Client httpClient;
  final SharedPreferences sharedPreferences;
  final String _baseUrl = 'https://demo-video-ws-chfmsoli4q-ew.a.run.app';

  const FavoriteListRemoteDataSourceImpl({
    @required this.httpClient,
    @required this.sharedPreferences,
  });

  @override
  Future<List<FavoriteListMovieModel>> getFavoriteMovies() async {
    final String token = sharedPreferences.getString(TOKEN);
    final Response response = await httpClient
        .get('$_baseUrl/video-ws/videos', headers: {'token': token});
    final responseJson = json.decode(response.body);
    if (response.statusCode == 200) {
      List<FavoriteListMovieModel> result = (responseJson as List)
          .map((movie) => FavoriteListMovieModel.fromJson(movie))
          .toList();
      result.sort((a, b) => b.timestamp.compareTo(a.timestamp));
      return result;
    }
    throw (responseJson['error']);
  }
}
