import 'package:demo_app/data/favorite_list/models/favorite_list_movie_model.dart';

abstract class FavoriteListRemoteDataSource {
  Future<List<FavoriteListMovieModel>> getFavoriteMovies();
}
