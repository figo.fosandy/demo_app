import 'package:demo_app/common/networks/network_info.dart';
import 'package:demo_app/data/favorite_list/datasources/favorite_list_remote_data_source.dart';
import 'package:demo_app/domain/favorite_list/entities/dashboard_movie_entity.dart';
import 'package:demo_app/domain/favorite_list/repositories/dashboard_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@RegisterAs(FavoriteListRepository)
@lazySingleton
@injectable
class FavoriteListRepositoryImpl implements FavoriteListRepository {
  final FavoriteListRemoteDataSource favoriteListRemoteDataSource;
  final NetworkInfo networkInfo;

  const FavoriteListRepositoryImpl({
    @required this.favoriteListRemoteDataSource,
    @required this.networkInfo,
  });

  @override
  Future<List<FavoriteListMovieEntity>> getFavoriteMovies() async {
    if (await networkInfo.isConnected()) {
      return await favoriteListRemoteDataSource.getFavoriteMovies();
    }
    throw ('Not connected to internet');
  }
}
