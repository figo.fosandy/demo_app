import 'package:demo_app/data/search/models/search_movie_model.dart';

abstract class SearchRemoteDataSource {
  Future<List<SearchMovieModel>> searchMovies({String keyword, String year});
}
