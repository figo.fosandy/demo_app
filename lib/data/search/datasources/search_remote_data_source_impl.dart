import 'dart:convert';

import 'package:demo_app/data/search/datasources/search_remote_data_source.dart';
import 'package:demo_app/data/search/models/search_movie_model.dart';
import 'package:injectable/injectable.dart';
import 'package:http/http.dart';
import 'package:meta/meta.dart';

@RegisterAs(SearchRemoteDataSource)
@lazySingleton
@injectable
class SearchRemoteDataSourceImpl implements SearchRemoteDataSource {
  final Client httpClient;
  final String _baseUrl = 'http://www.omdbapi.com';
  final String _apiKey = '770abde6';

  const SearchRemoteDataSourceImpl({@required this.httpClient});

  @override
  Future<List<SearchMovieModel>> searchMovies(
      {String keyword, String year}) async {
    final Response response = await httpClient.get(
        '$_baseUrl/?apikey=$_apiKey&type=movie&s=$keyword${year != 'all' ? '&y=$year' : ''}');
    final responseJson = json.decode(response.body);
    if (response.statusCode == 200) {
      if (responseJson['Response'] == 'False') {
        throw (responseJson['Error']);
      }
      return (responseJson['Search'] as List)
          .map((movie) => SearchMovieModel.fromJson(movie))
          .toList();
    }
    throw (responseJson['error']);
  }
}
