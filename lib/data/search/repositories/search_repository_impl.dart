import 'package:demo_app/common/networks/network_info.dart';
import 'package:demo_app/common/utils/search_cache.dart';
import 'package:demo_app/data/search/datasources/search_remote_data_source.dart';
import 'package:demo_app/domain/search/entities/search_movie_entity.dart';
import 'package:demo_app/domain/search/respositories/search_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@RegisterAs(SearchRepository)
@lazySingleton
@injectable
class SearchRepositoryImpl implements SearchRepository {
  final SearchCache searchCache;
  final SearchRemoteDataSource searchRemoteDataSource;
  final NetworkInfo networkInfo;

  const SearchRepositoryImpl({
    @required this.searchCache,
    @required this.searchRemoteDataSource,
    @required this.networkInfo,
  });

  @override
  Future<List<SearchMovieEntity>> searchMovies({
    String keyword,
    String year,
  }) async {
    if (await networkInfo.isConnected()) {
      if (searchCache.contains(keyword, year)) {
        return searchCache.get(keyword, year);
      }
      final result = await searchRemoteDataSource.searchMovies(
        keyword: keyword,
        year: year,
      );
      searchCache.set(keyword, year, result);
      return result;
    }
    throw ('Not connected to internet');
  }
}
