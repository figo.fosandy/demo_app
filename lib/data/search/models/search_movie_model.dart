import 'package:demo_app/domain/search/entities/search_movie_entity.dart';

class SearchMovieModel extends SearchMovieEntity {
  const SearchMovieModel({
    String id,
    String title,
    String year,
    String poster,
    String label,
    int priority,
    bool viewed,
    int rating,
    int timestamp,
  }) : super(
          id: id,
          title: title,
          year: year,
          poster: poster,
          label: label,
          priority: priority,
          viewed: viewed,
          rating: rating,
          timestamp: timestamp,
        );

  factory SearchMovieModel.fromJson(Map<String, dynamic> json) =>
      SearchMovieModel(
        id: json['imdbID'],
        title: json['Title'],
        year: json['Year'],
        poster: json['Poster'],
        label: '',
        priority: 2,
        viewed: false,
        rating: 0,
        timestamp: 0,
      );

  Map toJson() => {
        'id': id,
        'title': title,
        'year': year,
        'poster': poster,
        'label': label,
        'priority': priority,
        'viewed': viewed,
        'rating': rating,
        'timestamp': timestamp,
      };

  @override
  String toString() {
    return toJson().toString();
  }
}
