import 'package:demo_app/common/route_configs/router.dart';
import 'package:demo_app/common/route_configs/routes.dart';
import 'package:flutter/material.dart';

class App extends StatelessWidget {
  const App({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark(),
      initialRoute: Routes.splash,
      onGenerateRoute: Router.onGenerateRoute,
    );
  }
}
