import 'package:demo_app/common/usecases/usecase.dart';
import 'package:demo_app/domain/home/repositories/home_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@lazySingleton
@injectable
class RemoveCurrentToken implements UseCase<void, NoParams> {
  final HomeRepository homeRepository;

  const RemoveCurrentToken({@required this.homeRepository});

  @override
  Future<void> call(NoParams _) async {
    return await homeRepository.removeCurrentToken();
  }
}
