abstract class LoginRepository {
  Future<String> tryToLogin(String token);
  Future<void> saveTheToken(String token);
}
