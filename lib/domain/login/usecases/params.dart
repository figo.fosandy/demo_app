import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class Params extends Equatable {
  final String token;

  const Params({@required this.token});

  @override
  List<Object> get props => [token];
}
