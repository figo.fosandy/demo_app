import 'package:demo_app/common/usecases/usecase.dart';
import 'package:demo_app/domain/login/repositories/login_repository.dart';
import 'package:demo_app/domain/login/usecases/params.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@injectable
@lazySingleton
class TryToLogin implements UseCase<String, Params> {
  final LoginRepository loginRepository;

  const TryToLogin({@required this.loginRepository});

  @override
  Future<String> call(Params params) async {
    return await loginRepository.tryToLogin(params.token);
  }
}
