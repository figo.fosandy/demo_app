import 'package:demo_app/common/usecases/usecase.dart';
import 'package:demo_app/domain/login/repositories/login_repository.dart';
import 'package:demo_app/domain/login/usecases/params.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@injectable
@lazySingleton
class SaveTheToken implements UseCase<void, Params> {
  final LoginRepository loginRepository;

  const SaveTheToken({@required this.loginRepository});

  @override
  Future<void> call(Params params) async {
    return await loginRepository.saveTheToken(params.token);
  }
}
