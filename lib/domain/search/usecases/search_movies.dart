import 'package:demo_app/common/usecases/usecase.dart';
import 'package:demo_app/domain/search/entities/search_movie_entity.dart';
import 'package:demo_app/domain/search/respositories/search_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@lazySingleton
@injectable
class SearchMovies implements UseCase<List<SearchMovieEntity>, Params> {
  final SearchRepository searchRepository;

  const SearchMovies({@required this.searchRepository});

  @override
  Future<List<SearchMovieEntity>> call(Params params) {
    return searchRepository.searchMovies(
      keyword: params.keyword,
      year: params.year,
    );
  }
}

class Params extends Equatable {
  final String keyword;
  final String year;

  const Params({
    @required this.keyword,
    @required this.year,
  });

  @override
  List<Object> get props => [keyword, year];
}
