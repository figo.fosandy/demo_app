import 'package:demo_app/domain/search/entities/search_movie_entity.dart';

abstract class SearchRepository {
  Future<List<SearchMovieEntity>> searchMovies({String keyword, String year});
}
