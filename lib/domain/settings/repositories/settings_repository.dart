abstract class SettingsRepository {
  Future<void> setShowRecommended(bool showRecommended);
  Future<bool> getShowRecommended();
}
