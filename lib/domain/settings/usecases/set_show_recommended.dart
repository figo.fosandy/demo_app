import 'package:demo_app/common/usecases/usecase.dart';
import 'package:demo_app/domain/settings/repositories/settings_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@injectable
@lazySingleton
class SetShowRecommended implements UseCase<void, Params> {
  final SettingsRepository settingsRepository;

  const SetShowRecommended({@required this.settingsRepository});

  @override
  Future<void> call(Params params) async {
    return await settingsRepository.setShowRecommended(params.showRecommended);
  }
}

class Params extends Equatable {
  final bool showRecommended;

  const Params({@required this.showRecommended});

  @override
  List<Object> get props => [showRecommended];
}
