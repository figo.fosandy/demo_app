import 'package:demo_app/common/usecases/usecase.dart';
import 'package:demo_app/domain/settings/repositories/settings_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@injectable
@lazySingleton
class GetShowRecommended implements UseCase<bool, NoParams> {
  final SettingsRepository settingsRepository;

  const GetShowRecommended({@required this.settingsRepository});

  @override
  Future<bool> call(NoParams _) async {
    return await settingsRepository.getShowRecommended();
  }
}
