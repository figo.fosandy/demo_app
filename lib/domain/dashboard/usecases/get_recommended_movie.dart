import 'package:demo_app/common/usecases/usecase.dart';
import 'package:demo_app/domain/dashboard/entities/dashboard_movie_entity.dart';
import 'package:demo_app/domain/dashboard/repositories/dashboard_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@lazySingleton
@injectable
class GetRecommendedMovie implements UseCase<DashboardMovieEntity, NoParams> {
  final DashboardRepository dashboardRepository;

  const GetRecommendedMovie({@required this.dashboardRepository});

  @override
  Future<DashboardMovieEntity> call(NoParams params) async {
    return await dashboardRepository.getRecommendedMovie();
  }
}
