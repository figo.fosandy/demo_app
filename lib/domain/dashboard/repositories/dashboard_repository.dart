import 'package:demo_app/domain/dashboard/entities/dashboard_movie_entity.dart';

abstract class DashboardRepository {
  Future<DashboardMovieEntity> getRecommendedMovie();
}