import 'package:demo_app/common/models/movie_model.dart';
import 'package:demo_app/common/usecases/usecase.dart';
import 'package:demo_app/domain/favorite_detail/entities/favorite_detail_entity.dart';
import 'package:demo_app/domain/favorite_detail/repositories/favorite_detail_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@lazySingleton
@injectable
class UpdateCurrentMovieDetail
    implements UseCase<FavoriteDetailEntity, CurrentUpdateParams> {
  final FavoriteDetailRepository favoriteDetailRepository;

  const UpdateCurrentMovieDetail({@required this.favoriteDetailRepository});

  @override
  Future<FavoriteDetailEntity> call(CurrentUpdateParams params) async {
    return favoriteDetailRepository.updateCurrentMovieDetail(
      currentMovie: params.currentMovie,
      label: params.label,
      priority: params.priority,
      rating: params.rating,
      viewed: params.viewed,
    );
  }
}

class CurrentUpdateParams extends Equatable {
  final MovieModel currentMovie;
  final String label;
  final int priority;
  final int rating;
  final bool viewed;

  const CurrentUpdateParams({
    this.label,
    this.priority,
    this.rating,
    this.viewed,
    @required this.currentMovie,
  });

  @override
  List<Object> get props => [currentMovie, label, priority, rating];
}
