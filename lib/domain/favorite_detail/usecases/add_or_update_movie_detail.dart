import 'package:demo_app/common/models/movie_model.dart';
import 'package:demo_app/common/usecases/usecase.dart';
import 'package:demo_app/domain/favorite_detail/repositories/favorite_detail_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@lazySingleton
@injectable
class AddOrUpdateMovieDetail implements UseCase<void, Params> {
  final FavoriteDetailRepository favoriteDetailRepository;

  const AddOrUpdateMovieDetail({@required this.favoriteDetailRepository});

  @override
  Future<void> call(Params params) async {
    return await favoriteDetailRepository.addOrUpdateMovieDetail(
        movieDetail: params.movieDetail);
  }
}

class Params extends Equatable {
  final MovieModel movieDetail;

  const Params({@required this.movieDetail});

  @override
  List<Object> get props => [movieDetail];
}
