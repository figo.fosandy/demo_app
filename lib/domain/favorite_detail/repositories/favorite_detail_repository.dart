import 'package:demo_app/common/models/movie_model.dart';
import 'package:demo_app/domain/favorite_detail/entities/favorite_detail_entity.dart';
import 'package:meta/meta.dart';

abstract class FavoriteDetailRepository {
  FavoriteDetailEntity updateCurrentMovieDetail({
    @required MovieModel currentMovie,
    String label,
    int priority,
    int rating,
    bool viewed,
  });

  Future<void> addOrUpdateMovieDetail(
      {@required MovieModel movieDetail});
}
