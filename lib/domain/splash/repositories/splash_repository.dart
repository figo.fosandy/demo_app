abstract class SplashRepository {
  Future<bool> checkIsAuthenticated();
}