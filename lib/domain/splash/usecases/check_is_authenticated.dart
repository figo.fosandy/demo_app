import 'package:demo_app/common/usecases/usecase.dart';
import 'package:demo_app/domain/splash/repositories/splash_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@injectable
@lazySingleton
class CheckIsAuthenticated implements UseCase<bool, NoParams> {
  final SplashRepository splashRepository;

  const CheckIsAuthenticated({@required this.splashRepository});

  @override
  Future<bool> call(NoParams params) async {
    return await splashRepository.checkIsAuthenticated();
  }
}
