import 'package:demo_app/common/models/movie_model.dart';

class FavoriteListMovieEntity extends MovieModel {
  const FavoriteListMovieEntity({
    String id,
    String title,
    String year,
    String poster,
    String label,
    int priority,
    bool viewed,
    int rating,
    int timestamp,
  }) : super(
          id: id,
          title: title,
          year: year,
          poster: poster,
          label: label,
          priority: priority,
          viewed: viewed,
          rating: rating,
          timestamp: timestamp,
        );
}
