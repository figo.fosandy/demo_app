import 'package:demo_app/domain/favorite_list/entities/dashboard_movie_entity.dart';

abstract class FavoriteListRepository {
  Future<List<FavoriteListMovieEntity>> getFavoriteMovies();
}
