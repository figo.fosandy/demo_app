import 'package:demo_app/common/usecases/usecase.dart';
import 'package:demo_app/domain/favorite_list/entities/dashboard_movie_entity.dart';
import 'package:demo_app/domain/favorite_list/repositories/dashboard_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@lazySingleton
@injectable
class GetFavoriteMovies
    implements UseCase<List<FavoriteListMovieEntity>, NoParams> {
  final FavoriteListRepository favoriteListRepository;

  const GetFavoriteMovies({@required this.favoriteListRepository});

  @override
  Future<List<FavoriteListMovieEntity>> call(NoParams params) async {
    return await favoriteListRepository.getFavoriteMovies();
  }
}
