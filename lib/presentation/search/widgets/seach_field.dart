import 'package:flutter/material.dart';

class SearchField extends StatefulWidget {
  final void Function(String) onChanged;
  final void Function() onCleared;

  const SearchField({
    Key key,
    @required this.onChanged,
    @required this.onCleared,
  }) : super(key: key);

  @override
  _SearchFieldState createState() => _SearchFieldState();
}

class _SearchFieldState extends State<SearchField> {
  TextEditingController _searchController = TextEditingController();
  String _value = '';

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
        controller: _searchController,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(20),
          hintText: 'Type to search...',
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          suffixIcon: _value.isEmpty
              ? null
              : IconButton(
                  icon: Icon(Icons.cancel),
                  onPressed: () {
                    WidgetsBinding.instance.addPostFrameCallback((_) {
                      _searchController.clear();
                      setState(() {
                        _value = '';
                      });
                      widget.onCleared();
                    });
                  },
                ),
        ),
        onChanged: (text) {
          widget.onChanged(text);
          setState(() {
            _value = text;
          });
        });
  }
}
