import 'package:demo_app/presentation/search/widgets/action_button.dart';
import 'package:flutter/material.dart';

extension on Widget {
  Widget toDialog() => Dialog(child: this);
  Widget addFlexible() => Flexible(child: this);
  Widget addBorder() => Container(
        child: this,
        width: 100,
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.grey,
            style: BorderStyle.solid,
            width: 1,
          ),
          borderRadius: BorderRadius.circular(10),
        ),
      );
}

class YearDropDownField extends StatefulWidget {
  final Function() onAllChoosed;
  final Function(String) onYearChoosed;
  final int firstYear;
  final int lastYear;

  const YearDropDownField({
    Key key,
    @required this.onAllChoosed,
    @required this.onYearChoosed,
    @required this.firstYear,
    @required this.lastYear,
  }) : super(key: key);

  @override
  _YearDropDownFieldState createState() => _YearDropDownFieldState();
}

class _YearDropDownFieldState extends State<YearDropDownField> {
  String _value = 'all';

  Widget _yearListTopBar() => AppBar(
        automaticallyImplyLeading: false,
        title: Text('Choose a year'),
      );

  void _yearListDialog({
    @required Function(DateTime) onYearChoosed,
    @required DateTime firstYear,
    @required DateTime selectedYear,
    @required DateTime lastYear,
  }) =>
      showDialog(
        context: context,
        builder: (BuildContext context) => Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            _yearListTopBar(),
            YearPicker(
              selectedDate: selectedYear,
              onChanged: (choosedYear) {
                Navigator.pop(context);
                onYearChoosed(choosedYear);
              },
              firstDate: firstYear,
              lastDate: lastYear,
            ).addFlexible(),
            ButtonBar(
              children: <Widget>[
                ActionButton(
                  onPressed: () {
                    setState(() {
                      _value = 'all';
                    });
                    widget.onAllChoosed();
                  },
                  title: 'all',
                  context: context,
                ),
                ActionButton(
                  onPressed: () {},
                  title: 'cancel',
                  context: context,
                ),
              ],
            )
          ],
        ).toDialog(),
      );

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: () {
        _yearListDialog(
          onYearChoosed: (yearChoosed) {
            setState(() {
              _value = yearChoosed.year.toString();
            });
            widget.onYearChoosed(yearChoosed.year.toString());
          },
          firstYear: DateTime(widget.firstYear),
          selectedYear:
              _value == 'all' ? DateTime.now() : DateTime(int.parse(_value)),
          lastYear: DateTime(widget.lastYear),
        );
      },
      padding: EdgeInsets.all(0),
      child: Row(
        children: <Widget>[
          Icon(
            Icons.date_range,
            color: Colors.grey,
          ),
          Text(
            ' $_value',
            style: TextStyle(color: Colors.grey),
          ),
        ],
      ).addBorder(),
    );
  }
}
