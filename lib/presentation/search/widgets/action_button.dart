import 'package:flutter/material.dart';

class ActionButton extends StatelessWidget {
  final void Function() onPressed;
  final String title;
  final BuildContext context;

  const ActionButton(
      {Key key,
      @required this.onPressed,
      @required this.title,
      @required this.context})
      : super(key: key);

  @override
  Widget build(BuildContext buildContext) {
    return FlatButton(
      onPressed: () {
        Navigator.pop(context);
        onPressed();
      },
      child: Text(title),
    );
  }
}
