import 'package:demo_app/common/models/movie_model.dart';
import 'package:demo_app/common/widgets/base_movie_card.dart';
import 'package:flutter/material.dart';

class MovieCard extends StatelessWidget {
  final MovieModel movieModel;

  const MovieCard({Key key, @required this.movieModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseMovieCard(
      movieModel: movieModel,
      hideLabel: true,
      hidePriority: true,
      hideRating: true,
    );
  }
}
