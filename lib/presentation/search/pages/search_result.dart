import 'package:demo_app/common/widgets/loading_indicator.dart';
import 'package:demo_app/presentation/search/blocs/blocs.dart';
import 'package:demo_app/presentation/search/pages/search_result_empty.dart';
import 'package:demo_app/presentation/search/pages/search_result_exist.dart';
import 'package:demo_app/presentation/search/pages/search_result_failure.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

extension on Widget {
  Widget addVisibility({@required visible}) => Visibility(
        child: this,
        visible: visible,
      );

  Widget addFixPosition() => Positioned(
        child: this,
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
      );
}

class SearchResult extends StatelessWidget {
  final String keyword;
  final String year;

  const SearchResult({
    Key key,
    @required this.keyword,
    @required this.year,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SearchBloc, SearchState>(
      builder: (context, state) {
        return Stack(
          children: <Widget>[
            SearchResultExist(
                    movieList:
                        state is SearchStateSuccess ? state.props[0] : [])
                .addVisibility(visible: state is SearchStateSuccess),
            SearchResultFailure(
              errorMessage: state is SearchStateFailure ? state.props[0] : '',
              callback: () {
                context.bloc<SearchBloc>().add(SearchTermChanged(
                      keyword: keyword,
                      year: year,
                    ));
              },
            ).addVisibility(visible: state is SearchStateFailure),
            SearchResultEmpty()
                .addVisibility(visible: state is SearchStateEmpty),
            LoadingIndicator(message: 'Searching')
                .addVisibility(visible: state is SearchStateLoading),
          ],
        );
      },
    );
  }
}
