import 'package:demo_app/common/widgets/message_page.dart';
import 'package:flutter/material.dart';

class SearchResultEmpty extends MessagePage {
  const SearchResultEmpty();
  @override
  String message() => 'Hold on, seems you don\'t type\nthe keyword to search';

  @override
  IconData iconData() => Icons.search;
}
