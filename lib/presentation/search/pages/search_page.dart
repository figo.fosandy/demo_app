import 'package:demo_app/presentation/search/blocs/search_bloc.dart';
import 'package:demo_app/presentation/search/blocs/search_event.dart';
import 'package:demo_app/presentation/search/pages/search_result.dart';
import 'package:demo_app/presentation/search/widgets/seach_field.dart';
import 'package:demo_app/presentation/search/widgets/year_dropdown_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

extension on Widget {
  Widget addFlexible() => Flexible(child: this);

  Widget addPadding({@required EdgeInsets padding}) => Padding(
        child: this,
        padding: padding,
      );
}

class SearchPage extends StatefulWidget {
  const SearchPage();

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  String _keyword = '';
  String _year = 'all';

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            SearchField(
              onChanged: (text) {
                _keyword = text;
                context.bloc<SearchBloc>().add(SearchTermChanged(
                      keyword: text,
                      year: _year,
                    ));
              },
              onCleared: () {
                _keyword = '';
                context.bloc<SearchBloc>().add(SearchTermChanged(
                      keyword: '',
                      year: _year,
                    ));
              },
            ).addFlexible(),
            YearDropDownField(
              firstYear: DateTime.now().year - 10,
              lastYear: DateTime.now().year + 5,
              onAllChoosed: () {
                _year = 'all';
                context.bloc<SearchBloc>().add(SearchTermChanged(
                      keyword: _keyword,
                      year: 'all',
                    ));
              },
              onYearChoosed: (year) {
                _year = year;
                context.bloc<SearchBloc>().add(SearchTermChanged(
                      keyword: _keyword,
                      year: year,
                    ));
              },
            ).addPadding(padding: const EdgeInsets.only(left: 5)),
          ],
        ).addPadding(padding: const EdgeInsets.all(10)),
        SearchResult(
          keyword: _keyword,
          year: _year,
        ).addFlexible(),
      ],
    );
  }
}
