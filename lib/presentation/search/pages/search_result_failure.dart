import 'package:demo_app/common/widgets/message_page.dart';
import 'package:flutter/material.dart';

extension on Widget {
  Widget addVisibility({@required bool visible}) => Visibility(
        visible: visible,
        child: this,
      );
}

class SearchResultFailure extends MessagePage {
  final String errorMessage;
  final void Function() callback;

  const SearchResultFailure({
    Key key,
    @required this.errorMessage,
    @required this.callback,
  }) : super(key: key);

  @override
  String message() => errorMessage == 'Not connected to internet'
      ? errorMessage
      : '$errorMessage\nPlease, use another keyword';

  @override
  Color baseColor() =>
      errorMessage == 'Not connected to internet' ? Colors.grey : Colors.red;

  @override
  IconData iconData() => errorMessage == 'Not connected to internet'
      ? Icons.signal_wifi_off
      : Icons.cancel;

  @override
  Widget action(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.refresh),
      onPressed: callback,
    ).addVisibility(visible: errorMessage == 'Not connected to internet');
  }
}
