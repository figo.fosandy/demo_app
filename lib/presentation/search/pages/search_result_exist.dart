import 'package:demo_app/common/models/movie_model.dart';
import 'package:demo_app/presentation/search/widgets/movie_card.dart';
import 'package:flutter/material.dart';

class SearchResultExist extends StatelessWidget {
  final List<MovieModel> movieList;

  const SearchResultExist({
    Key key,
    @required this.movieList,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemCount: movieList.length,
      separatorBuilder: (context, index) => Divider(
        color: Colors.transparent,
      ),
      itemBuilder: (context, index) => MovieCard(
        movieModel: movieList[index],
      ),
    );
  }
}
