import 'package:demo_app/common/models/movie_model.dart';
import 'package:demo_app/domain/search/usecases/search_movies.dart';
import 'package:demo_app/presentation/search/blocs/blocs.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:stream_transform/stream_transform.dart';

@lazySingleton
@injectable
class SearchBloc extends Bloc<SearchEvent, SearchState> {
  final SearchMovies searchMovies;

  SearchBloc({@required this.searchMovies});

  @override
  Stream<SearchState> transformEvents(
    Stream<SearchEvent> events,
    Stream<SearchState> Function(SearchEvent event) next,
  ) {
    return super.transformEvents(
      events.debounce(const Duration(milliseconds: 500)),
      next,
    );
  }

  @override
  SearchState get initialState => SearchStateEmpty();

  @override
  Stream<SearchState> mapEventToState(SearchEvent event) async* {
    if (event is SearchTermChanged) {
      final String keyword = event.keyword;
      final String year = event.year;
      if (keyword.isEmpty) {
        yield SearchStateEmpty();
      } else {
        yield SearchStateLoading();
        await Future.delayed(const Duration(seconds: 1));
        try {
          final List<MovieModel> result =
              await searchMovies(Params(keyword: keyword, year: year));
          yield SearchStateSuccess(movieList: result);
        } catch (error) {
          yield SearchStateFailure(error: error.toString());
        }
      }
    }
  }
}
