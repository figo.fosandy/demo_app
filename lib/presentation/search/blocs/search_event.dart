import 'package:equatable/equatable.dart';

abstract class SearchEvent extends Equatable {
  const SearchEvent();
}

class SearchTermChanged extends SearchEvent {
  final String keyword;
  final String year;

  const SearchTermChanged({this.keyword, this.year});

  @override
  List<Object> get props => [keyword, year];

  @override
  String toString() => 'SearchTermChanged { keyword: $keyword, year: $year}';
}
