import 'package:demo_app/common/models/movie_model.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class SearchState extends Equatable {
  const SearchState();

  @override
  List<Object> get props => [];
}

class SearchStateEmpty extends SearchState {}

class SearchStateLoading extends SearchState {}

class SearchStateSuccess extends SearchState {
  final List<MovieModel> movieList;

  const SearchStateSuccess({@required this.movieList});

  @override
  List<Object> get props => [movieList];

  @override
  String toString() => 'SearchStateSuccess { result: $movieList }';
}

class SearchStateFailure extends SearchState {
  final String error;

  const SearchStateFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SearchStateFailure { error: $error }';
}
