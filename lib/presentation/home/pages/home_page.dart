import 'package:demo_app/common/route_configs/routes.dart';
import 'package:demo_app/presentation/home/blocs/bloc.dart';
import 'package:demo_app/presentation/home/widgets/home_page_with_loading.dart';
import 'package:demo_app/presentation/login/blocs/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatelessWidget {
  final int currentPageIndex;

  const HomePage({Key key, this.currentPageIndex}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomeBloc, HomeState>(
      listener: (context, state) {
        if (state is HomeLoggedOut) {
          Navigator.of(context).pushReplacementNamed(Routes.login);
          context.bloc<LoginBloc>().add(LoginReset());
        }
      },
      builder: (context, state) => HomePageWithLoading(
        currentPageIndex: currentPageIndex ?? 0,
        isLoading: state is HomeLogoutLoading,
      ),
    );
  }
}
