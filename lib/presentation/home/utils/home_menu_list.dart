import 'package:demo_app/common/route_configs/routes.dart';
import 'package:flutter/material.dart';

abstract class HomeMenuList {
  const HomeMenuList();
  static const dashboardMenu = {
    'name': 'Dashboard',
    'icon': Icons.home,
    'route': Routes.dashboard,
  };
  static const searchMenu = {
    'name': 'Search',
    'icon': Icons.search,
    'route': Routes.search,
  };
  static const favoriteMenu = {
    'name': 'Favorite',
    'icon': Icons.star,
    'route': Routes.favoriteList,
  };
  static const settingsMenu = {
    'name': 'Settings',
    'icon': Icons.settings,
    'route': Routes.settings
  };
}
