import 'package:demo_app/common/widgets/base_page.dart';
import 'package:demo_app/presentation/dashboard/pages/dashboard_page.dart';
import 'package:demo_app/presentation/favorite_list/pages/favorite_page.dart';
import 'package:demo_app/presentation/home/blocs/bloc.dart';
import 'package:demo_app/presentation/home/utils/home_menu_list.dart';
import 'package:demo_app/presentation/search/blocs/blocs.dart';
import 'package:demo_app/presentation/search/pages/search_page.dart';
import 'package:demo_app/presentation/settings/pages/settings_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePageChild extends BasePage {
  static const List<Map> menuList = [
    HomeMenuList.dashboardMenu,
    HomeMenuList.searchMenu,
    HomeMenuList.favoriteMenu,
    HomeMenuList.settingsMenu,
  ];

  final int currentPageIndex;

  const HomePageChild({@required this.currentPageIndex});

  @override
  bool useDynamicAppBar() => true;

  @override
  Widget title() {
    return Text(menuList[currentPageIndex]['name']);
  }

  @override
  Key pageKey() => Key(menuList[currentPageIndex]['name']);

  @override
  Widget body(BuildContext context) {
    return IndexedStack(
      index: currentPageIndex,
      children: <Widget>[
        DashboardPage(),
        SearchPage(),
        FavoritePage(),
        SettingsPage(
          callback: () {
            context.bloc<HomeBloc>().add(LogoutButtonPressed());
          },
        ),
      ],
    );
  }

  @override
  Widget drawer(BuildContext rootContext) {
    return Drawer(
      child: ListView.separated(
        separatorBuilder: (context, index) => Divider(),
        itemCount: menuList.length,
        itemBuilder: (context, index) => ListTile(
          enabled: currentPageIndex != index,
          leading: Icon(menuList[index]['icon']),
          title: Text(menuList[index]['name']),
          onTap: () {
            Navigator.pop(context);
            Navigator.of(rootContext)
                .pushReplacementNamed(menuList[index]['route']);
            if (currentPageIndex == 1) {
              rootContext
                  .bloc<SearchBloc>()
                  .add(SearchTermChanged(keyword: '', year: 'all'));
            }
          },
        ),
      ),
    );
  }

  @override
  Widget bottomNavigationBar(BuildContext rootContext) {
    return BottomNavigationBar(
      currentIndex: currentPageIndex,
      items: List.from(menuList)
          .map((menu) => BottomNavigationBarItem(
                icon: Icon(menu['icon']),
                title: Text(menu['name']),
              ))
          .toList(),
      onTap: (index) {
        if (index != currentPageIndex) {
          Navigator.of(rootContext)
              .pushReplacementNamed(menuList[index]['route']);
          if (currentPageIndex == 1) {
            rootContext
                .bloc<SearchBloc>()
                .add(SearchTermChanged(keyword: '', year: 'all'));
          }
        }
      },
    );
  }
}
