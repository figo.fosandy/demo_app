import 'package:demo_app/presentation/home/widgets/home_loading_page.dart';
import 'package:demo_app/presentation/home/widgets/home_page_child.dart';
import 'package:flutter/material.dart';

class HomePageWithLoading extends StatelessWidget {
  final int currentPageIndex;
  final bool isLoading;

  const HomePageWithLoading({
    Key key,
    @required this.currentPageIndex,
    @required this.isLoading,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IndexedStack(
      index: isLoading ? 1 : 0,
      children: <Widget>[
        HomePageChild(currentPageIndex: currentPageIndex),
        HomeLoadingPage(),
      ],
    );
  }
}
