import 'package:demo_app/common/usecases/usecase.dart';
import 'package:demo_app/domain/home/usecases/remove_current_token.dart';
import 'package:demo_app/presentation/home/blocs/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@lazySingleton
@injectable
class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final RemoveCurrentToken removeCurrentToken;

  HomeBloc({
    @required this.removeCurrentToken,
  });

  @override
  HomeState get initialState => HomeInitial();

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    if (event is LogoutButtonPressed) {
      yield HomeLogoutLoading();
      await Future.delayed(const Duration(seconds: 1));
      await removeCurrentToken(NoParams());
      yield HomeLoggedOut();
    }
    if (event is HomeReset) {
      yield HomeInitial();
    }
  }
}
