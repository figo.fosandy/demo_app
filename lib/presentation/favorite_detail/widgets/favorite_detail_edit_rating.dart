import 'package:demo_app/common/configs/injector.dart';
import 'package:demo_app/presentation/favorite_detail/blocs/blocs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class FavoriteDetailEditRating extends StatelessWidget {
  final int rating;

  const FavoriteDetailEditRating({
    Key key,
    @required this.rating,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RatingBar(
      initialRating: rating / 2,
      minRating: 0,
      direction: Axis.horizontal,
      allowHalfRating: true,
      itemCount: 5,
      itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
      itemBuilder: (context, _) => Icon(
        Icons.star,
        color: Colors.cyan,
      ),
      onRatingUpdate: (double rating) {
        getIt<FavoriteDetailBloc>().add(
            FavoriteDetailUpdateCurrentMovie(rating: (rating * 2).round()));
      },
    );
  }
}
