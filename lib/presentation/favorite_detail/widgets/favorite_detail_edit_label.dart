import 'package:demo_app/common/configs/injector.dart';
import 'package:demo_app/presentation/favorite_detail/blocs/favorite_detail_bloc.dart';
import 'package:demo_app/presentation/favorite_detail/blocs/favorite_detail_event.dart';
import 'package:flutter/material.dart';

class FavoriteDetailEditLabel extends StatefulWidget {
  final String label;

  const FavoriteDetailEditLabel({
    Key key,
    @required this.label,
  }) : super(key: key);

  @override
  _FavoriteDetailEditLabelState createState() =>
      _FavoriteDetailEditLabelState();
}

class _FavoriteDetailEditLabelState extends State<FavoriteDetailEditLabel> {
  bool _isLabelEdit = false;
  FocusNode _labelFocusNode = FocusNode();
  TextEditingController _labelController;

  @override
  void initState() {
    setState(() {
      _labelController = TextEditingController(text: widget.label ?? '');
    });
    super.initState();
  }

  @override
  void dispose() {
    _labelController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: TextFormField(
            focusNode: _labelFocusNode,
            enabled: _isLabelEdit,
            onFieldSubmitted: (_) {
              getIt<FavoriteDetailBloc>().add(FavoriteDetailUpdateCurrentMovie(
                  label: _labelController.text));
              setState(() {
                _isLabelEdit = false;
              });
            },
            controller: _labelController,
            decoration: InputDecoration(
              hintText: 'label',
            ),
          ),
        ),
        IconButton(
          icon: Icon(_isLabelEdit ? Icons.check : Icons.edit),
          onPressed: () {
            WidgetsBinding.instance.addPostFrameCallback((_) {
              FocusScope.of(context).requestFocus(_labelFocusNode);
            });
            if (_isLabelEdit) {
              getIt<FavoriteDetailBloc>().add(FavoriteDetailUpdateCurrentMovie(
                  label: _labelController.text));
            }
            setState(() {
              _isLabelEdit = !_isLabelEdit;
            });
          },
        ),
      ],
    );
  }
}
