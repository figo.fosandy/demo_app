import 'package:demo_app/common/configs/injector.dart';
import 'package:demo_app/presentation/favorite_detail/blocs/favorite_detail_bloc.dart';
import 'package:demo_app/presentation/favorite_detail/blocs/favorite_detail_event.dart';
import 'package:flutter/material.dart';

const Map<int, String> _priorityFormula = {
  0: 'high',
  1: 'medium',
  2: 'low',
};

class FavoriteDetailEditPriority extends StatelessWidget {
  final int priority;

  const FavoriteDetailEditPriority({
    Key key,
    @required this.priority,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
        children: List<Widget>.generate(
            3,
            (int index) => ChoiceChip(
                  selectedColor: Colors.cyan,
                  label: Text(_priorityFormula[index]),
                  selected: priority == index,
                  labelStyle: TextStyle(color: Colors.white),
                  onSelected: (bool selected) {
                    if (selected) {
                      getIt<FavoriteDetailBloc>().add(
                          FavoriteDetailUpdateCurrentMovie(priority: index));
                    }
                  },
                )).reversed.toList());
  }
}
