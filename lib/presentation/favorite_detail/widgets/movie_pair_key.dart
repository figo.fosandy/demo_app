import 'package:flutter/material.dart';

extension on Widget {
  Widget addPadding({@required EdgeInsets padding}) => Padding(
        child: this,
        padding: padding,
      );
}

class MoviePairKey extends StatelessWidget {
  final String pairKey;

  const MoviePairKey({
    Key key,
    @required this.pairKey,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      pairKey,
      style: TextStyle(
        color: Colors.white,
        fontWeight: FontWeight.bold,
        fontSize: 20,
      ),
    ).addPadding(padding: EdgeInsets.only(top: 10));
  }
}
