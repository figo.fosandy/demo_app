import 'package:demo_app/common/configs/injector.dart';
import 'package:demo_app/presentation/favorite_detail/blocs/favorite_detail_bloc.dart';
import 'package:demo_app/presentation/favorite_detail/blocs/favorite_detail_event.dart';
import 'package:flutter/material.dart';

class FavoriteDetailEditViewed extends StatelessWidget {
  final bool viewed;

  const FavoriteDetailEditViewed({
    Key key,
    @required this.viewed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChoiceChip(
      selectedColor: Colors.cyan,
      label: Text('${viewed ? 'V' : 'Unv'}iewed'),
      avatar: Icon(Icons.visibility),
      labelStyle: TextStyle(color: Colors.white),
      selected: viewed,
      onSelected: (bool selected) {
        getIt<FavoriteDetailBloc>()
            .add(FavoriteDetailUpdateCurrentMovie(viewed: !viewed));
      },
    );
  }
}
