import 'package:demo_app/common/configs/injector.dart';
import 'package:demo_app/common/models/movie_model.dart';
import 'package:demo_app/presentation/favorite_detail/blocs/blocs.dart';
import 'package:demo_app/presentation/favorite_detail/widgets/favorite_detail_edit_label.dart';
import 'package:demo_app/presentation/favorite_detail/widgets/favorite_detail_edit_priority.dart';
import 'package:demo_app/presentation/favorite_detail/widgets/favorite_detail_edit_rating.dart';
import 'package:demo_app/presentation/favorite_detail/widgets/favorite_detail_edit_viewed.dart';
import 'package:demo_app/presentation/favorite_detail/widgets/movie_pair_key.dart';
import 'package:flutter/material.dart';

class FavoriteDetailEditingPageChild extends StatelessWidget {
  final MovieModel movieModel;

  const FavoriteDetailEditingPageChild({
    Key key,
    @required this.movieModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.all(10),
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            MoviePairKey(pairKey: 'Label'),
            FavoriteDetailEditLabel(label: movieModel.label),
            MoviePairKey(pairKey: 'Priority'),
            FavoriteDetailEditPriority(priority: movieModel.priority),
            MoviePairKey(pairKey: 'Rating'),
            FavoriteDetailEditRating(rating: movieModel.rating),
            MoviePairKey(pairKey: 'Viewed'),
            FavoriteDetailEditViewed(viewed: movieModel.viewed),
            Visibility(
                visible: getIt<FavoriteDetailBloc>().state
                    is FavoriteDetailCurrentUpdate,
                child: Align(
                  alignment: Alignment.bottomRight,
                  child: MaterialButton(
                    onPressed: () {
                      getIt<FavoriteDetailBloc>()
                          .add(FavoriteDetailAddOrUpdateToFavoriteList());
                    },
                    child: Text('Add or Update'),
                    color: Colors.blue,
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
