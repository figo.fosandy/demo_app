import 'package:demo_app/domain/favorite_detail/entities/favorite_detail_entity.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class FavoriteDetailState extends Equatable {
  final FavoriteDetailEntity movie;
  final String status;
  final String error;

  const FavoriteDetailState({
    FavoriteDetailEntity movie,
    String status,
    String error,
  })  : this.movie = movie ?? const FavoriteDetailEntity(),
        this.status = status ?? 'initial',
        this.error = error ?? '';

  @override
  List<Object> get props => [movie, status];
}

class FavoriteDetailInitial extends FavoriteDetailState {
  const FavoriteDetailInitial({FavoriteDetailEntity movie})
      : super(movie: movie);
}

class FavoriteDetailUpdatingLoading extends FavoriteDetailState {
  const FavoriteDetailUpdatingLoading({@required FavoriteDetailEntity movie})
      : super(
          movie: movie,
          status: 'loading',
        );
}

class FavoriteDetailUpdatingSuccess extends FavoriteDetailState {
  const FavoriteDetailUpdatingSuccess({@required FavoriteDetailEntity movie})
      : super(
          movie: movie,
          status: 'success',
        );
}

class FavoriteDetailCurrentUpdate extends FavoriteDetailState {
  const FavoriteDetailCurrentUpdate({@required FavoriteDetailEntity movie})
      : super(
          movie: movie,
          status: 'current update',
        );
}

class FavoriteDetailUpdatingFailure extends FavoriteDetailState {
  const FavoriteDetailUpdatingFailure({
    @required FavoriteDetailEntity movie,
    @required String error,
  }) : super(
          movie: movie,
          status: 'error',
          error: error,
        );
}
