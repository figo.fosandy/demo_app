import 'package:demo_app/common/models/movie_model.dart';
import 'package:equatable/equatable.dart';

abstract class FavoriteDetailEvent extends Equatable {
  const FavoriteDetailEvent();

  @override
  List<Object> get props => [];
}

class FavoriteDetailUpdateCurrentMovie extends FavoriteDetailEvent {
  final String label;
  final int priority;
  final int rating;
  final bool viewed;

  const FavoriteDetailUpdateCurrentMovie({
    this.label,
    this.priority,
    this.rating,
    this.viewed,
  }) : assert(!(label == null &&
            priority == null &&
            rating == null &&
            viewed == null));

  @override
  List<Object> get props => [
        label,
        priority,
        rating,
        viewed,
      ];
}

class FavoriteDetailAddOrUpdateToFavoriteList extends FavoriteDetailEvent {}

class FavoriteDetailSetCurrentMovie extends FavoriteDetailEvent {
  final MovieModel movie;
  const FavoriteDetailSetCurrentMovie({this.movie});

  @override
  List<Object> get props => [movie];
}

class FavoriteDetailResetStatus extends FavoriteDetailEvent {}
