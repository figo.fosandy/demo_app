import 'package:demo_app/domain/favorite_detail/entities/favorite_detail_entity.dart';
import 'package:demo_app/domain/favorite_detail/usecases/add_or_update_movie_detail.dart';
import 'package:demo_app/domain/favorite_detail/usecases/update_current_movie_detail.dart';
import 'package:demo_app/presentation/favorite_detail/blocs/blocs.dart';
import 'package:demo_app/presentation/favorite_list/blocs/blocs.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@lazySingleton
@injectable
class FavoriteDetailBloc
    extends Bloc<FavoriteDetailEvent, FavoriteDetailState> {
  final AddOrUpdateMovieDetail addOrUpdateMovieDetail;
  final UpdateCurrentMovieDetail updateCurrentMovieDetail;
  final FavoriteListBloc favoriteListBloc;

  FavoriteDetailBloc({
    @required this.addOrUpdateMovieDetail,
    @required this.updateCurrentMovieDetail,
    @required this.favoriteListBloc,
  });

  @override
  FavoriteDetailState get initialState => FavoriteDetailInitial();

  @override
  Stream<FavoriteDetailState> mapEventToState(
      FavoriteDetailEvent event) async* {
    if (event is FavoriteDetailSetCurrentMovie) {
      final FavoriteDetailEntity movie = await updateCurrentMovieDetail(
          CurrentUpdateParams(currentMovie: event.movie));
      yield FavoriteDetailInitial(movie: movie);
    } else if (event is FavoriteDetailUpdateCurrentMovie) {
      final FavoriteDetailEntity movie = await updateCurrentMovieDetail(
          CurrentUpdateParams(
              currentMovie: state.movie,
              label: event.label,
              priority: event.priority,
              rating: event.rating,
              viewed: event.viewed));
      yield FavoriteDetailCurrentUpdate(movie: movie);
    } else if (event is FavoriteDetailAddOrUpdateToFavoriteList) {
      yield FavoriteDetailUpdatingLoading(movie: state.movie);
      Future.delayed(const Duration(seconds: 1));
      try {
        await addOrUpdateMovieDetail(Params(movieDetail: state.movie));
        yield FavoriteDetailUpdatingSuccess(movie: state.movie);
        favoriteListBloc.add(FavoriteListFetchingStarted());
      } catch (error) {
        yield FavoriteDetailUpdatingFailure(
            movie: state.movie, error: error.toString());
      }
    } else if (event is FavoriteDetailResetStatus) {
      yield FavoriteDetailInitial(movie: state.movie);
    }
  }
}
