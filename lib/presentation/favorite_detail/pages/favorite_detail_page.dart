import 'package:demo_app/common/models/movie_model.dart';
import 'package:demo_app/common/widgets/base_page.dart';
import 'package:demo_app/common/widgets/loading_indicator.dart';
import 'package:demo_app/presentation/favorite_detail/blocs/blocs.dart';
import 'package:demo_app/presentation/favorite_detail/pages/favorite_detail_editing_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

extension on Widget {
  Widget addVisibility({@required bool visible}) => Visibility(
        child: this,
        visible: visible,
      );
}

class FavoriteDetailPage extends BasePage {
  final MovieModel movie;

  const FavoriteDetailPage({@required this.movie});

  @override
  String pageName() => 'Movie Detail Page';

  @override
  Widget body(BuildContext context) {
    return BlocBuilder<FavoriteDetailBloc, FavoriteDetailState>(
        builder: (context, state) {
      return Stack(
        children: <Widget>[
          FavoriteDetailEditingPage(movie: state.movie),
          LoadingIndicatorWithoutScafold(
            message: 'Updating to favorite list',
            backgroundColor: Colors.black54,
          ).addVisibility(visible: state is FavoriteDetailUpdatingLoading)
        ],
      );
    });
  }
}
