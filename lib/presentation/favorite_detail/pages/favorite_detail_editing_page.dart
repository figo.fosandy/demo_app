import 'package:demo_app/common/configs/injector.dart';
import 'package:demo_app/common/models/movie_model.dart';
import 'package:demo_app/presentation/favorite_detail/blocs/blocs.dart';
import 'package:demo_app/presentation/favorite_detail/widgets/favorite_detail_editing_page_child.dart';
import 'package:demo_app/presentation/favorite_detail/widgets/movie_card.dart';
import 'package:flutter/material.dart';

extension on Widget {
  Widget addVisibility({@required bool visible}) => Visibility(
        child: this,
        visible: visible,
      );
}

class FavoriteDetailEditingPage extends StatefulWidget {
  final MovieModel movie;

  const FavoriteDetailEditingPage({
    Key key,
    @required this.movie,
  }) : super(key: key);

  @override
  _FavoriteDetailEditingPageState createState() =>
      _FavoriteDetailEditingPageState();
}

class _FavoriteDetailEditingPageState extends State<FavoriteDetailEditingPage> {
  @override
  void dispose() {
    getIt<FavoriteDetailBloc>().add(FavoriteDetailResetStatus());
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(children: <Widget>[
      MovieCard(movieModel: widget.movie),
      FavoriteDetailEditingPageChild(movieModel: widget.movie),
    ]);
  }
}
