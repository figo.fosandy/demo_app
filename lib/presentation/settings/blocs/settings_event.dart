import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class SettingsEvent extends Equatable {
  const SettingsEvent();

  @override
  List<Object> get props => [];
}

class SettingsSetShowRecommeded extends SettingsEvent {
  final bool showRecommended;

  const SettingsSetShowRecommeded({
    @required this.showRecommended,
  });

  @override
  List<Object> get props => [showRecommended];
}

class SettingsGetShowRecommeded extends SettingsEvent {}
