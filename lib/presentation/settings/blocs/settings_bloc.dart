import 'dart:async';

import 'package:demo_app/common/usecases/usecase.dart';
import 'package:demo_app/domain/settings/usecases/get_show_recommended.dart';
import 'package:demo_app/domain/settings/usecases/set_show_recommended.dart';
import 'package:demo_app/presentation/settings/blocs/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@injectable
@lazySingleton
class SettingsBloc extends Bloc<SettingsEvent, bool> {
  final SetShowRecommended setShowRecommended;
  final GetShowRecommended getShowRecommended;

  SettingsBloc({
    @required this.getShowRecommended,
    @required this.setShowRecommended,
  });

  @override
  bool get initialState => true;

  @override
  Stream<bool> mapEventToState(SettingsEvent event) async* {
    if (event is SettingsGetShowRecommeded) {
      yield await getShowRecommended(NoParams());
    } else if (event is SettingsSetShowRecommeded) {
      await setShowRecommended(Params(showRecommended: event.showRecommended));
      yield event.showRecommended;
    }
  }
}
