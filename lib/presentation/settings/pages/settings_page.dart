import 'package:demo_app/presentation/settings/blocs/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SettingsPage extends StatelessWidget {
  final void Function() callback;

  const SettingsPage({
    Key key,
    @required this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsBloc, bool>(
      builder: (context, state) => Column(
        children: <Widget>[
          SwitchListTile(
            title: Text('Show Recommended'),
            value: state,
            onChanged: (bool showRecommended) {
              context.bloc<SettingsBloc>().add(
                  SettingsSetShowRecommeded(showRecommended: showRecommended));
            },
          ),
          RaisedButton(
            child: Text('LOGOUT'),
            onPressed: callback,
          ),
        ],
      ),
    );
  }
}
