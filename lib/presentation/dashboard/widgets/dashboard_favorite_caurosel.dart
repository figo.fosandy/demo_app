import 'package:carousel_slider/carousel_slider.dart';
import 'package:demo_app/common/models/movie_model.dart';
import 'package:demo_app/presentation/dashboard/widgets/dashboard_empty_favorite.dart';
import 'package:demo_app/presentation/dashboard/widgets/movie_card.dart';
import 'package:flutter/material.dart';

extension on Widget {
  Widget addVisibility({
    @required bool visible,
    @required Widget replacement,
  }) =>
      Visibility(
        visible: visible,
        child: this,
        replacement: replacement,
      );
}

class DashboardFavoriteCaurosel extends StatefulWidget {
  final List<MovieModel> movieList;

  const DashboardFavoriteCaurosel({
    Key key,
    @required this.movieList,
  }) : super(key: key);

  @override
  _DashboardFavoriteCauroselState createState() =>
      _DashboardFavoriteCauroselState();
}

class _DashboardFavoriteCauroselState extends State<DashboardFavoriteCaurosel> {
  int _current = 0;

  @override
  Widget build(BuildContext context) {
    widget.movieList.sort((a, b) => a.priority.compareTo(b.priority));
    return Stack(
      children: [
        CarouselSlider.builder(
          itemCount: widget.movieList.length > 3 ? 3 : widget.movieList.length,
          itemBuilder: (BuildContext context, int index) => MovieCard(
            movieModel: widget.movieList[index],
          ),
          autoPlay: true,
          height: 250,
          viewportFraction: 1.0,
          onPageChanged: (index) {
            setState(() {
              _current = index;
            });
          },
        ),
        Positioned(
          bottom: 0.0,
          left: 0.0,
          right: 0.0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: widget.movieList
                .take(3)
                .map(
                  (movie) => Container(
                    width: 8.0,
                    height: 8.0,
                    margin:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: _current == widget.movieList.indexOf(movie)
                            ? Color.fromRGBO(0, 255, 255, 0.9)
                            : Color.fromRGBO(0, 255, 255, 0.4)),
                  ),
                )
                .toList(),
          ),
        )
      ],
    ).addVisibility(
        visible: widget.movieList.isNotEmpty,
        replacement: DashboardEmptyFavoriteList());
  }
}
