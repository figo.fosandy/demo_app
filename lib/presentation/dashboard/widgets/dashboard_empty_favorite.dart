import 'package:demo_app/common/widgets/message_page.dart';

class DashboardEmptyFavoriteList extends MessagePage {
  const DashboardEmptyFavoriteList();
  @override
  String message() =>
      'Hold on, seems you don\'t have\nany unviewed favorite videos';
}
