import 'package:demo_app/common/widgets/loading_indicator.dart';
import 'package:demo_app/presentation/dashboard/blocs/blocs.dart';
import 'package:demo_app/presentation/dashboard/pages/dashboard_fetch_error.dart';
import 'package:demo_app/presentation/dashboard/pages/dashboard_fetch_success_page.dart';
import 'package:demo_app/presentation/favorite_list/blocs/blocs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

extension on Widget {
  Widget addVisibility({@required visible}) =>
      Visibility(child: this, visible: visible);
}

class DashboardPage extends StatelessWidget {
  const DashboardPage();
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DashboardBloc, DashboardState>(
      builder: (context, state) {
        return BlocBuilder<FavoriteListBloc, FavoriteListState>(
          builder: (context, favoriteState) {
            return Stack(
              children: <Widget>[
                DashboardFetchSuccessPage(
                        state: state, favoriteState: favoriteState)
                    .addVisibility(
                        visible: state is DashboardFetchingSuccess &&
                            favoriteState is FavoriteListFetchingSuccess),
                DashboardFetchError(
                        errorMessage: state is DashboardFetchingFailure
                            ? state.props[0]
                            : favoriteState is FavoriteListFetchingFailure
                                ? favoriteState.props[0]
                                : '')
                    .addVisibility(
                        visible: state is DashboardFetchingFailure ||
                            favoriteState is FavoriteListFetchingFailure),
                LoadingIndicatorWithoutScafold(message: 'Fetching the data')
                    .addVisibility(
                        visible: !(state is DashboardFetchingFailure ||
                                favoriteState is FavoriteListFetchingFailure) &&
                            !(state is DashboardFetchingSuccess &&
                                favoriteState is FavoriteListFetchingSuccess)),
              ],
            );
          },
        );
      },
    );
  }
}
