import 'package:demo_app/common/widgets/message_page.dart';
import 'package:demo_app/presentation/dashboard/blocs/blocs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DashboardFetchError extends MessagePage {
  final String errorMessage;

  const DashboardFetchError({@required this.errorMessage});

  @override
  Color baseColor() => Colors.red;

  @override
  IconData iconData() => Icons.cancel;

  @override
  String message() => 'Oops, Something Went Wrong\n$errorMessage';

  @override
  Widget action(BuildContext context) {
    return IconButton(
        icon: Icon(Icons.refresh),
        onPressed: () {
          context.bloc<DashboardBloc>().add(DashboardFetchingStarted());
        });
  }
}
