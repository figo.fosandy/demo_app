import 'dart:async';

import 'package:demo_app/common/models/movie_model.dart';
import 'package:demo_app/data/dashboard/models/dashboard_movie_model.dart';
import 'package:demo_app/presentation/dashboard/blocs/blocs.dart';
import 'package:demo_app/presentation/dashboard/widgets/dashboard_favorite_caurosel.dart';
import 'package:demo_app/presentation/dashboard/widgets/movie_card.dart';
import 'package:demo_app/presentation/favorite_list/blocs/blocs.dart';
import 'package:demo_app/presentation/settings/blocs/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

extension on Widget {
  Widget addPading({@required EdgeInsets padding}) => Padding(
        padding: padding,
        child: this,
      );

  Widget addRefreshIndicator({@required Future<void> Function() onRefresh}) =>
      RefreshIndicator(child: this, onRefresh: onRefresh);

  Widget addVisibility({@required bool visible}) => Visibility(
        child: this,
        visible: visible,
      );
}

extension on Text {
  Widget toBig() => Text(
        this.data,
        style: TextStyle(
          fontSize: 30,
          color: Colors.white,
          fontWeight: FontWeight.bold,
        ),
      );
}

class DashboardFetchSuccessPage extends StatefulWidget {
  final DashboardState state;
  final FavoriteListState favoriteState;

  const DashboardFetchSuccessPage({
    Key key,
    @required this.state,
    @required this.favoriteState,
  }) : super(key: key);

  @override
  _DashboardFetchSuccessPageState createState() =>
      _DashboardFetchSuccessPageState();
}

class _DashboardFetchSuccessPageState extends State<DashboardFetchSuccessPage> {
  Completer<void> _refreshCompleter;

  @override
  void initState() {
    super.initState();
    _refreshCompleter = Completer<void>();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsBloc, bool>(
      builder: (context, state) => ListView(
        children: <Widget>[
          Text('Recommended')
              .toBig()
              .addPading(padding: const EdgeInsets.only(left: 20))
              .addVisibility(visible: state),
          MovieCard(
                  movieModel: widget.state.props.isEmpty ||
                          widget.state.props[0] is String
                      ? DashboardMovieModel()
                      : widget.state.props[0])
              .addVisibility(visible: state),
          Text('Top Favorites')
              .toBig()
              .addPading(padding: const EdgeInsets.only(left: 20)),
          DashboardFavoriteCaurosel(
            movieList: widget.favoriteState.props.isEmpty ||
                    widget.favoriteState.props[0] is String
                ? []
                : (widget.favoriteState.props[0] as List<MovieModel>)
                    .where((movie) => !movie.viewed)
                    .toList(),
          )
        ],
      ).addRefreshIndicator(onRefresh: () {
        context.bloc<DashboardBloc>().add(DashboardFetchingStarted());
        return _refreshCompleter.future;
      }),
    );
  }
}
