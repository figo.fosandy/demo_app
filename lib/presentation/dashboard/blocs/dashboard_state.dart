import 'package:demo_app/common/models/movie_model.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class DashboardState extends Equatable {
  const DashboardState();

  @override
  List<Object> get props => [];
}

class DashboardInitial extends DashboardState {}

class DashboardFetchingLoading extends DashboardState {}

class DashboardFetchingSuccess extends DashboardState {
  final MovieModel movie;

  const DashboardFetchingSuccess({@required this.movie});

  @override
  List<Object> get props => [movie];

  @override
  String toString() => 'DashboardFetchingSuccess { result: $movie }';
}

class DashboardFetchingFailure extends DashboardState {
  final String error;

  const DashboardFetchingFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'DashboardFetchingFailure { error: $error }';
}
