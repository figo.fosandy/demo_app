import 'package:demo_app/common/models/movie_model.dart';
import 'package:demo_app/common/usecases/usecase.dart';
import 'package:demo_app/domain/dashboard/usecases/get_recommended_movie.dart';
import 'package:demo_app/presentation/dashboard/blocs/blocs.dart';
import 'package:demo_app/presentation/favorite_list/blocs/blocs.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@lazySingleton
@injectable
class DashboardBloc extends Bloc<DashboardEvent, DashboardState> {
  final GetRecommendedMovie getRecommendedMovie;
  final FavoriteListBloc favoriteListBloc;

  DashboardBloc({
    @required this.getRecommendedMovie,
    @required this.favoriteListBloc,
  });

  @override
  DashboardState get initialState => DashboardInitial();

  @override
  Stream<DashboardState> mapEventToState(DashboardEvent event) async* {
    if (event is DashboardFetchingStarted) {
      yield DashboardFetchingLoading();
      await Future.delayed(const Duration(seconds: 1));
      try {
        MovieModel movie = await getRecommendedMovie(NoParams());
        yield DashboardFetchingSuccess(movie: movie);
        favoriteListBloc.add(FavoriteListFetchingStarted());
      } catch (error) {
        yield DashboardFetchingFailure(error: error.toString());
      }
    }
  }
}
