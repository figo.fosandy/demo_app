import 'dart:async';

import 'package:demo_app/domain/login/usecases/params.dart';
import 'package:demo_app/domain/login/usecases/save_the_token.dart';
import 'package:demo_app/domain/login/usecases/try_to_login.dart';
import 'package:demo_app/presentation/dashboard/blocs/blocs.dart';
import 'package:demo_app/presentation/login/blocs/bloc.dart';
import 'package:demo_app/presentation/settings/blocs/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@injectable
@lazySingleton
class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final TryToLogin tryToLogin;
  final SaveTheToken saveTheToken;
  final DashboardBloc dashboardBloc;
  final SettingsBloc settingsBloc;

  LoginBloc({
    @required this.tryToLogin,
    @required this.saveTheToken,
    @required this.dashboardBloc,
    @required this.settingsBloc,
  });

  @override
  LoginState get initialState => LoginInitial();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginButtonPressed) {
      yield TryToLoginLoading();

      await Future.delayed(const Duration(seconds: 1));
      try {
        final token = await tryToLogin(Params(token: event.token));
        yield SaveTheTokenLoading();
        await Future.delayed(const Duration(seconds: 1));
        await saveTheToken(Params(token: token));
        settingsBloc.add(SettingsGetShowRecommeded());
        yield LoginSuccess();
        dashboardBloc.add(DashboardFetchingStarted());
      } catch (error) {
        yield LoginFailure(error: error.toString());
      }
    }
    if (event is LoginReset) {
      yield LoginInitial();
    }
  }
}
