import 'package:demo_app/presentation/login/widgets/login_form.dart';
import 'package:demo_app/presentation/login/widgets/login_loading_page.dart';
import 'package:flutter/cupertino.dart';

class LoginPageWithLoading extends StatelessWidget {
  final bool isSaveTheTokenLoading;

  const LoginPageWithLoading({
    Key key,
    @required this.isSaveTheTokenLoading,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IndexedStack(
      index: isSaveTheTokenLoading ? 1 : 0,
      children: <Widget>[
        LoginForm(),
        LoginLoadingPage(),
      ],
    );
  }
}
