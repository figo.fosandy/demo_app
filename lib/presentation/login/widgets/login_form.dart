import 'package:demo_app/common/widgets/loading_indicator.dart';
import 'package:demo_app/presentation/login/blocs/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

extension on Widget {
  Widget addPadding({EdgeInsets padding}) {
    return Padding(
      padding: padding,
      child: this,
    );
  }

  Widget addLoadingIndicator({bool isLoading}) {
    return Stack(
      children: <Widget>[
        this,
        Visibility(
          child: LoadingIndicatorWithoutScafold(
            backgroundColor: Colors.black54,
          ),
          visible: isLoading,
        ),
      ],
    );
  }
}

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _tokenController = TextEditingController();

  @override
  void dispose() {
    _tokenController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state is LoginFailure) {
          print('called');
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text('${state.error}'),
              backgroundColor: Colors.red,
            ),
          );
        }
      },
      builder: (context, state) {
        return Form(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              TextFormField(
                decoration: const InputDecoration(labelText: 'token'),
                controller: _tokenController,
              ),
              RaisedButton(
                onPressed: () {
                  context
                      .bloc<LoginBloc>()
                      .add(LoginButtonPressed(token: _tokenController.text));
                },
                child: const Text('Login'),
              ),
            ],
          ),
        )
            .addPadding(padding: const EdgeInsets.symmetric(horizontal: 25))
            .addLoadingIndicator(isLoading: state is TryToLoginLoading);
      },
    );
  }
}
