import 'package:demo_app/common/widgets/loading_indicator.dart';
import 'package:flutter/material.dart';

class LoginLoadingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LoadingIndicator(message: 'Logging in');
  }
}
