import 'package:demo_app/common/route_configs/routes.dart';
import 'package:demo_app/common/widgets/base_page.dart';
import 'package:demo_app/presentation/home/blocs/bloc.dart';
import 'package:demo_app/presentation/login/blocs/bloc.dart';
import 'package:demo_app/presentation/login/widgets/login_page_with_loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginPage extends BasePage {
  @override
  Widget body(BuildContext context) {
    return BlocConsumer<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state is LoginSuccess) {
          Navigator.of(context).pushReplacementNamed(Routes.home);
          context.bloc<HomeBloc>().add(HomeReset());
        }
      },
      builder: (context, state) => LoginPageWithLoading(
          isSaveTheTokenLoading: state is SaveTheTokenLoading),
    );
  }
}
