import 'dart:async';

import 'package:demo_app/common/models/movie_model.dart';
import 'package:demo_app/presentation/favorite_list/blocs/blocs.dart';
import 'package:demo_app/presentation/favorite_list/widgets/movie_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

extension on Widget {
  Widget addRefreshIndicator({@required Future<void> Function() onRefresh}) =>
      RefreshIndicator(child: this, onRefresh: onRefresh);
}

class FavoriteResultExist extends StatefulWidget {
  final List<MovieModel> movieList;

  const FavoriteResultExist({
    Key key,
    @required this.movieList,
  }) : super(key: key);

  @override
  _FavoriteResultExistState createState() => _FavoriteResultExistState();
}

class _FavoriteResultExistState extends State<FavoriteResultExist> {
  Completer<void> _refreshCompleter;

  @override
  void initState() {
    super.initState();
    _refreshCompleter = Completer<void>();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemBuilder: (context, index) =>
          MovieCard(movieModel: widget.movieList[index]),
      separatorBuilder: (context, index) => Divider(color: Colors.transparent),
      itemCount: widget.movieList.length,
    ).addRefreshIndicator(onRefresh: () {
      context.bloc<FavoriteListBloc>().add(FavoriteListFetchingStarted());
      return _refreshCompleter.future;
    });
  }
}
