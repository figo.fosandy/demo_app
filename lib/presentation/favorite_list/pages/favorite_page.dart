import 'package:demo_app/common/widgets/loading_indicator.dart';
import 'package:demo_app/presentation/favorite_list/blocs/blocs.dart';
import 'package:demo_app/presentation/favorite_list/pages/favorite_result_empty.dart';
import 'package:demo_app/presentation/favorite_list/pages/favorite_result_exist.dart';
import 'package:demo_app/presentation/favorite_list/pages/favorite_result_failure.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

extension on Widget {
  Widget addVisibility({@required visible}) =>
      Visibility(child: this, visible: visible);
}

class FavoritePage extends StatelessWidget {
  const FavoritePage();
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FavoriteListBloc, FavoriteListState>(
      builder: (context, state) {
        return Stack(
          children: <Widget>[
            FavoriteResultExist(
                    movieList: state is FavoriteListFetchingSuccess
                        ? state.props[0]
                        : [])
                .addVisibility(visible: state is FavoriteListFetchingSuccess),
            FavoriteResultEmpty().addVisibility(
                visible: state is FavoriteListFetchingSuccess &&
                    (state.props[0] as List).isEmpty),
            FavoriteResultFailure(
                    errorMessage: state is FavoriteListFetchingFailure
                        ? state.props[0]
                        : '')
                .addVisibility(visible: state is FavoriteListFetchingFailure),
            LoadingIndicatorWithoutScafold(message: 'Fetching the data')
                .addVisibility(
                    visible: state is FavoriteListInitial ||
                        state is FavoriteListFetchingLoading),
          ],
        );
      },
    );
  }
}
