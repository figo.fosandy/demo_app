import 'package:demo_app/common/widgets/message_page.dart';

class FavoriteResultEmpty extends MessagePage {
  @override
  String message() => 'Hold on, seems you don\'t have\nany favorite videos';
}
