import 'package:demo_app/common/models/movie_model.dart';
import 'package:demo_app/common/usecases/usecase.dart';
import 'package:demo_app/domain/favorite_list/usecases/get_favorite_movies.dart';
import 'package:demo_app/presentation/favorite_list/blocs/blocs.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@lazySingleton
@injectable
class FavoriteListBloc extends Bloc<FavoriteListEvent, FavoriteListState> {
  final GetFavoriteMovies getFavoriteMovies;

  FavoriteListBloc({@required this.getFavoriteMovies});

  @override
  FavoriteListState get initialState => FavoriteListInitial();

  @override
  Stream<FavoriteListState> mapEventToState(FavoriteListEvent event) async* {
    if (event is FavoriteListFetchingStarted) {
      yield FavoriteListFetchingLoading();
      await Future.delayed(const Duration(seconds: 1));
      try {
        List<MovieModel> movieList = await getFavoriteMovies(NoParams());
        yield FavoriteListFetchingSuccess(movies: movieList);
      } catch (error) {
        yield FavoriteListFetchingFailure(error: error.toString());
      }
    }
  }
}
