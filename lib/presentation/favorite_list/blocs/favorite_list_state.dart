import 'package:demo_app/common/models/movie_model.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class FavoriteListState extends Equatable {
  const FavoriteListState();

  @override
  List<Object> get props => [];
}

class FavoriteListInitial extends FavoriteListState {}

class FavoriteListFetchingLoading extends FavoriteListState {}

class FavoriteListFetchingSuccess extends FavoriteListState {
  final List<MovieModel> movies;

  const FavoriteListFetchingSuccess({@required this.movies});

  @override
  List<Object> get props => [movies];

  @override
  String toString() => 'FavoriteListFetchingSuccess { result: $movies }';
}

class FavoriteListFetchingFailure extends FavoriteListState {
  final String error;

  const FavoriteListFetchingFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'FavoriteListFetchingFailure { error: $error }';
}
