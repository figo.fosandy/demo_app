import 'package:equatable/equatable.dart';

abstract class FavoriteListEvent extends Equatable {
  const FavoriteListEvent();

  @override
  List<Object> get props => [];
}

class FavoriteListFetchingStarted extends FavoriteListEvent {}
