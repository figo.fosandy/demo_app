import 'package:demo_app/common/widgets/base_page.dart';
import 'package:demo_app/presentation/splash/widgets/splash_loading_page.dart';
import 'package:flutter/material.dart';
import 'package:marquee_widget/marquee_widget.dart';

extension on Widget {
  Widget addPadding({EdgeInsets padding}) {
    return Padding(
      padding: padding,
      child: this,
    );
  }

  Widget addMarquee() {
    return Marquee(child: this);
  }

  Widget addMargin({EdgeInsets margin}) {
    return Container(
      margin: margin,
      child: this,
    );
  }

  Widget addLoadingIndicator({@required bool isLoading}) {
    return Stack(
      children: <Widget>[
        this,
        Visibility(
          child: SplashLoadingPage(),
          visible: isLoading,
        ),
      ],
    );
  }
}

class SplashPageChild extends BasePage {
  final bool isLoading;

  const SplashPageChild({@required this.isLoading});

  @override
  Key pageKey() => Key('splashPage');

  @override
  Widget body(BuildContext context) {
    return Container(
      alignment: Alignment.topCenter,
      child: Column(
        children: <Widget>[
          _circleLogo().addMargin(
            margin: EdgeInsets.only(bottom: 50, top: 100),
          ),
          _text().addMarquee().addMargin(
                margin: EdgeInsets.symmetric(horizontal: 25),
              )
        ],
      ).addLoadingIndicator(isLoading: isLoading),
    );
  }

  Widget _circleLogo() {
    return CircleAvatar(
      backgroundColor: Colors.white,
      child: _logo().addPadding(
        padding: const EdgeInsets.all(15),
      ),
      radius: 100,
    );
  }

  Widget _text() {
    return Text(
      'Place to keep your favorite movies',
      style: TextStyle(
        fontSize: 21,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  Widget _logo() {
    return Image.asset('assets/picture/logo.png');
  }
}
