import 'package:demo_app/common/widgets/loading_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SplashLoadingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LoadingIndicatorWithoutScafold(
      message: 'Application is preparing',
      backgroundColor: Colors.black54,
    );
  }
}
