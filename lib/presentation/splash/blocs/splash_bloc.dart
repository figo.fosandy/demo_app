import 'package:demo_app/common/usecases/usecase.dart';
import 'package:demo_app/domain/splash/usecases/check_is_authenticated.dart';
import 'package:demo_app/presentation/dashboard/blocs/blocs.dart';
import 'package:demo_app/presentation/splash/blocs/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@lazySingleton
@injectable
class SplashBloc extends Bloc<SplashEvent, SplashState> {
  final CheckIsAuthenticated checkIsAuthenticated;
  final DashboardBloc dashboardBloc;

  SplashBloc({
    @required this.checkIsAuthenticated,
    @required this.dashboardBloc,
  });

  @override
  SplashState get initialState => SplashInitial();

  @override
  Stream<SplashState> mapEventToState(SplashEvent event) async* {
    if (event is SplashCheckAuthenticated) {
      await Future.delayed(const Duration(seconds: 1));
      yield SplashLoading();

      final bool isAuthenticated = await checkIsAuthenticated(NoParams());
      await Future.delayed(const Duration(seconds: 1));
      if (isAuthenticated) {
        yield SplashAuthenticated();
        dashboardBloc.add(DashboardFetchingStarted());
      } else {
        yield SplashUnauthenticated();
      }
    }
  }
}
