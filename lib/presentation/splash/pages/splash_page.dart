import 'package:demo_app/common/route_configs/routes.dart';
import 'package:demo_app/presentation/home/blocs/bloc.dart';
import 'package:demo_app/presentation/login/blocs/bloc.dart';
import 'package:demo_app/presentation/splash/blocs/bloc.dart';
import 'package:demo_app/presentation/splash/widgets/splash_page_child.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SplashPage extends StatelessWidget {
  const SplashPage();
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<SplashBloc, SplashState>(
      listener: (context, state) {
        if (state is SplashUnauthenticated) {
          Navigator.of(context).pushReplacementNamed(Routes.login);
          context.bloc<LoginBloc>().add(LoginReset());
        } else if (state is SplashAuthenticated) {
          Navigator.pushReplacementNamed(context, Routes.home);
          context.bloc<HomeBloc>().add(HomeReset());
        }
      },
      builder: (context, state) =>
          SplashPageChild(isLoading: state is SplashLoading),
    );
  }
}
