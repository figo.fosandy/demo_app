import 'package:demo_app/app.dart';
import 'package:demo_app/bloc_delegate.dart';
import 'package:demo_app/common/configs/injector.dart';
import 'package:demo_app/presentation/dashboard/blocs/blocs.dart';
import 'package:demo_app/presentation/favorite_detail/blocs/blocs.dart';
import 'package:demo_app/presentation/favorite_list/blocs/blocs.dart';
import 'package:demo_app/presentation/home/blocs/bloc.dart';
import 'package:demo_app/presentation/login/blocs/bloc.dart';
import 'package:demo_app/presentation/settings/blocs/bloc.dart';
import 'package:demo_app/presentation/splash/blocs/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'presentation/search/blocs/blocs.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await configure();
  BlocSupervisor.delegate = AppBlocDelegate();
  runApp(MultiBlocProvider(
    providers: [
      BlocProvider<SplashBloc>(
        create: (_) => getIt<SplashBloc>()..add(SplashCheckAuthenticated()),
      ),
      BlocProvider<LoginBloc>(
        create: (_) => getIt<LoginBloc>(),
      ),
      BlocProvider<HomeBloc>(
        create: (_) => getIt<HomeBloc>(),
      ),
      BlocProvider<DashboardBloc>(
        create: (_) => getIt<DashboardBloc>(),
      ),
      BlocProvider<FavoriteListBloc>(
        create: (_) => getIt<FavoriteListBloc>(),
      ),
      BlocProvider<SearchBloc>(
        create: (_) => getIt<SearchBloc>(),
      ),
      BlocProvider<FavoriteDetailBloc>(
        create: (_) => getIt<FavoriteDetailBloc>(),
      ),
      BlocProvider<SettingsBloc>(
        create: (_) => getIt<SettingsBloc>()..add(SettingsGetShowRecommeded()),
      ),
    ],
    child: App(),
  ));
}
